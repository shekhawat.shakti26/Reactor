import React, { Component } from 'react';
// import { Scrollbars } from 'react-custom-scrollbars';
import './Module.css';
import * as closefunction from '../../Screens/screenNav';

var userApp_JSON = localStorage.getItem('JSON');
var AppJson = JSON.parse(userApp_JSON);

export default class Modules extends Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }
    AddModule(ModuleName) {
        if (ModuleName === "Login") {
            if (!AppJson["LoginModule"]) {
                AppJson["Screens"].push(
                    { "Name": "Login", "Path": "/News_App/screen/LoginScreen/" },
                    { "Name": "SignUp", "Path": "/News_App/screen/SignUpScreen" },
                )
                AppJson["LoginModule"] = {
                    "Layout": "Layout1",
                    "Logo": "https://firebasestorage.googleapis.com/v0/b/reactor-2f46b.appspot.com/o/default%2Flogo.jpg?alt=media&token=c0888311-ea7c-4d4d-9162-17170c5d9118",
                    "LogoText": "Your News",
                    "BackgroundImage":"https://firebasestorage.googleapis.com/v0/b/reactor-2f46b.appspot.com/o/rFuHFaJ07lch1JSH0FvXqIrUXNP2%2FmyApp222018183457%2Fsplash?alt=media&token=96f9aaad-41fc-4324-b3f4-fcea86f948ed",
                    "LoginScreen": {
                        "Title": "Welcome to News",
                        "components": {
                            "Textbox_Email": "Email",
                            "Textbox_Password": "Password",
                            "Button": "Log In",
                            "Bottom_Text": "Don't have account yet?",
                            "Bottom_Link": "SignUp"
                        }
                    },
                    "SignUpScreen": {
                        "Title": "Create Account",
                        "components": {
                            "Textbox_UserName": "Name",
                            "Textbox_Email": "Email",
                            "Textbox_Password": "Password",
                            "Textbox_Password2": "Confirm Password",
                            "Button": "Sign up",
                            "Bottom_Text": "Already have an account?",
                            "Bottom_Link": "Sign in"
                        }
                    }// signup screen ends
                }//login module ends
            }
        }//if ends
        if (ModuleName === "List") {
            if (!AppJson["ListViewModule"]) {
                AppJson["Screens"].push(
                    { "Name": "Detail", "Path": "/News_App/screen/NewsDescription" },
                    { "Name": "List", "Path": "/News_App/screen/NewsFeed" }
                )
                AppJson["ListViewModule"] = {
                    "Layout": "Layout1",
                    "ListView": {
                        "Title": "Top Stories",
                        "components": {}
                    },
                    "DetailView": {
                        "Title": "News",
                        "components": {}
                    }
                }
            }

        }//if ends
        console.log(AppJson);
        localStorage.setItem('JSON', JSON.stringify(AppJson));
        closefunction.CloseScreenModules();
        window.location.reload();
    }//addmodule function ends
    render() {
        return (<div>
            <div className="Item_Row">{/* 1 st Row starts */}

                <div className="Item_block" onClick={() => this.AddModule("Login")}> {/* 1 st item starts */}
                    <div className="Module">
                        <img className="Module_image" alt="..." src="https://extensions.static.shoutem.com/shoutem/news/1.5.2/server/assets/add-news-image.png" />
                    </div>
                    <div className="Module_Name">
                        <span className="BoldText">Authentication</span>
                        <span className="LightText">Include Login and SignUp Page</span>
                    </div>
                </div>                             {/* 1 st item ends */}

                <div className="Item_block" onClick={() => this.AddModule("List")}> {/* 2 st item starts */}
                    <div className="Module">
                        <img className="Module_image" alt="..." src="https://extensions.static.shoutem.com/shoutem/news/1.5.2/server/assets/add-news-image.png" />
                    </div>
                    <div className="Module_Name">
                        <span className="BoldText">List</span>
                        <span className="LightText">Include List Items and Description Page</span>
                    </div>
                </div>                       {/* 2 nd item ends */}

                <div className="Item_block"> 
                    <div className="Module">
                        <img className="Module_image" alt="..." src="https://extensions.static.shoutem.com/shoutem/news/1.5.2/server/assets/add-news-image.png" />
                    </div>
                    <div className="Module_Name">
                        <span className="BoldText">Login</span>
                        <span className="LightText">Include Login and SignUp Page</span>
                    </div> </div>

                <div className="Item_block">  
                    <div className="Module">
                        <img className="Module_image" alt="..." src="https://extensions.static.shoutem.com/shoutem/news/1.5.2/server/assets/add-news-image.png" />
                    </div>
                    <div className="Module_Name">
                        <span className="BoldText">Login</span>
                        <span className="LightText">Include Login and SignUp Page</span>
                    </div>
                </div>

            </div>  {/* 1 st Row ends */}

        </div>);
    }
}
export var AppJson = AppJson;