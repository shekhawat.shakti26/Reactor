import React from 'react';
import './AppMaker.css';
import Sidebar from './Sidebar';
import { Link } from 'react-router-dom';
import logo from '../../assets/logos/youstart.png';
import { Scrollbars } from 'react-custom-scrollbars';
import Preview from './preview/preview.js';
import firebaseApp from '../../FireBase';
import {
    RingLoader,
    // BarLoader, ScaleLoader, SyncLoader
} from 'react-spinners';

// var userId= localStorage.getItem('UserData');
var userApp_JSON = localStorage.getItem('JSON');
var AppJson = JSON.parse(userApp_JSON);

class AppMaker extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }

    on() {
        var box = document.getElementById('overlay');
        box.style.display = "block";
    }
    off() {
        var box = document.getElementById('overlay');
        box.style.display = "none";
    }
    componentDidMount() {
        console.log(AppJson);
    }
    async DownloadApk() {
        //   var userId =localStorage.getItem('UserData');
        var userApp_JSON = localStorage.getItem('JSON');
        var AppJson = JSON.parse(userApp_JSON);
        var AppRef = firebaseApp.database().ref('Projects/' + AppJson.userId + "/" + AppJson.projectId)
        var App_Data = [];
        AppRef.on('value', (snapshot) => {
            snapshot.forEach((child) => {
                App_Data.push({
                    data: child.val()
                });
            });
            var BuildApkURL = App_Data[0].data;
            window.location.assign(BuildApkURL)
        });



        // .once("value")
        // .then(function(snapshot) {
        //     console.log(snapshot);
        //      console.log(snapshot.val());
        //     // var BuildApk = (snapshot.val() && snapshot.val().BuildApk) || 'Anonymous';
        //    console.log("BuildApk");
        // //    console.log(BuildApk);
        //   });
    }
    Save() {
        var userID = localStorage.getItem('UserId');
        var userApp_JSON = localStorage.getItem('JSON');
        var AppJson = JSON.parse(userApp_JSON);
        if (userID) {
            firebaseApp.database().ref('Projects/' + userID + "/" + AppJson.projectId).set({ //store messages in database
                Name: AppJson.appName,
                ProjectId: AppJson.projectId,
                JSON: JSON.stringify(AppJson),
                BuildApk: "null"
            }).then(() => {
                localStorage.setItem('JSON', JSON.stringify(AppJson));
                console.log("AppJson");
                console.log(AppJson);
                window.location.reload();
            });
        }
    }
    SignOut() {
        firebaseApp.auth().signOut().then(function () {
            console.log("Sign-out successful");
            window.location = '/about';
        }).catch(function (error) {
            // An error happened.
        });
    }

    Build(urlLink) {
        var box = document.getElementById('overlay');
        box.style.display = "block";
        var userApp_JSON = localStorage.getItem('JSON');
        var AppJson = JSON.parse(userApp_JSON);
        console.log(AppJson);

        fetch(urlLink, {
            method: 'POST',
            mode: 'CORS',
            body: JSON.stringify(AppJson),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then((response) => response.json())
            .then(responseJson => {
                // var res=JSON.parse(response);
                console.log(responseJson)
                if (responseJson.message === "JSON received") {
                    console.log("Build Called")
                    this.Build("http://ec2-52-34-99-28.us-west-2.compute.amazonaws.com:8088/build");
                }
                if (responseJson.message === "Build completed") {
                    // alert("Build Successfull");
                    this.GetUrl();
                }
                if (responseJson.message === "Build not completed") {
                    box.style.display = "none";
                    alert("Build not Completed")
                }
            })
            .catch(err => {
                box.style.display = "none";
                console.log(err);
                alert(err)
            });
    }

    GetUrl() {
        var box = document.getElementById('overlay');
        console.log("getUrl");
        var userApp_JSON = localStorage.getItem('JSON');
        var AppJson = JSON.parse(userApp_JSON);
        console.log(AppJson);
        var userId = AppJson.userId;
        var projectId = AppJson.projectId;
        var storageRef = firebaseApp.storage().ref();
        storageRef.child(userId + "/" + projectId + "/" + "app-release.apk").getDownloadURL()
            .then(function (url) {
                console.log(url);
                if (url) {
                    console.log("geturlfunction");
                    console.log(url);
                    firebaseApp.database().ref('Projects/' + userId + "/" + AppJson.projectId).set({ //store messages in database
                        Name: AppJson.appName,
                        ProjectId: AppJson.projectId,
                        JSON: JSON.stringify(AppJson),
                        BuildApk: url
                    })
                    box.style.display = "none";
                }
            })
            .catch(function (error) {
                box.style.display = "none";
                console.log(error);
            });
    }
    render() {
        return (
            <div className="AppMaker-root" >
                {/* header of the page start */}
                <div className="AppMaker-top" >
                    <div className="AppMaker_logo-container">
                        <Link className="Navigation-link" to="/ui">
                            <img alt="" src={logo} style={{ width: 50, height: 50 }} />  </Link>
                    </div>
                    <div className="top_middle">
                    </div>

                    <div className="top_right">
                        <button className="Top_button" onClick={() => this.DownloadApk()} >Download</button>
                        <button className="Top_button" onClick={() => this.Build("http://ec2-52-34-99-28.us-west-2.compute.amazonaws.com:8088/AppData")}>Build</button>
                        <button className="Top_button" onClick={() => this.Save()}>Save</button>
                        <button className="Top_button" onClick={() => this.SignOut()}>SignOut</button>
                    </div>
                </div>
                {/* header of the page ends*/}
                <div id="overlay" className="overlay">
                    <div id="loader" className="loader">
                        <RingLoader style={{ marginLeft: 15, height: 500, width: 200 }} color={'#123abc'} loading={true} />
                        {/* <BarLoader style={{ marginLeft: 15 }} color={'#123abc'} loading={this.state.loading} /> */}
                        {/* <ScaleLoader style={{ marginLeft: 15 }} color={'#123abc'} loading={this.state.loading} /> */}
                        {/* <SyncLoader style={{ marginLeft: 15 }} color={'#123abc'} loading={this.state.loading} /> */}
                    </div>
                </div>
                <div className="AppMaker-Content">
                    <div className="AppMaker_content">
                        <div className="AppMaker_content-left">
                            {/* used Sidebar component from sidebar.js file */}
                            <Sidebar />
                        </div>
                        <div className="AppMaker_content-right">
                            <div className="AppMaker_Content_right_top">
                                <strong className="AppMaker-lineThrough">Preview</strong>
                                <p style={{ fontSize: 10 }}>Click on the screen to Refresh</p>
                            </div>
                            <Scrollbars style={{ height: '80%' }}>
                                <div className="AppMaker_DeviceMock">
                                    <div className="Post-thumbnail" style={{ height: 440, width: 240, backgroundColor: 'white' }}>
                                        {/* <Preview /> */}
                                    </div>
                                </div> </Scrollbars>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default AppMaker;