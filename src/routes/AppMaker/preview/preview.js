import React, { Component } from 'react';
// import { Link } from 'react-router-dom';
import { Scrollbars } from 'react-custom-scrollbars';
import * as login from './Modules/Login/LoginLayout.js';
import * as signup from './Modules/Login/SignUpLayout.js';
import * as listView from './Modules/ListView/ListViewLayout.js';
import * as description from './Modules/ListView/DescriptionLayout.js';
import './preview.css'
// import {Preview} from './Screens/Products/Products.js';
import * as appData from '../variableData';

export default class Preview extends Component {
    constructor(props) {
        super(props)
        this.state = {
            ScreenChosen: '',
            layoutChosen: ''
        }
    }
    componentDidMount() {
        console.log('show preview');
    }
    renderLayout = (screen, layout) => {
        var userApp_JSON = localStorage.getItem('JSON');
        var AppJson = JSON.parse(userApp_JSON);

        // alert("renderLayout called");
        console.log(layout);
        var data = "name";
        // var layout = appData.previewLayout;
        if (screen === 'Login') {
            if (layout === "Layout1") {
                return <login.Layout1 data={data} />
            }
            if (layout === "Layout2") {
                return <login.Layout2 data={data} />
            }
        }
        if (screen === 'SignUp') {
            if (layout === "Layout1") {
                return <signup.Layout1 data={data} />
            }
            if (layout === "Layout2") {
                return <signup.Layout2 data={data} />
            }
        }
        if (screen === 'List') {
            if (layout === "Layout1") {
                return <listView.Layout1 data={data} />
            }
            if (layout === "Layout2") {
                return <listView.Layout2 data={data} />
            }
        }
        if (screen === 'Detail') {
            if (layout === "Layout1") {
                return <description.Layout1 data={data} />
            }
            if (layout === "Layout2") {
                return <description.Layout2 data={data} />
            }
        }
    };
    selectLayout() {
        let Screen = appData.previewScreen;
        let layout = appData.previewLayout;
        this.setState({
            layoutChosen: layout,
            ScreenChosen: Screen
        });
        console.log("previewscreen select");
    }
    render() {
        return (
            <div className="productPreview" onClick={() => this.selectLayout()}>
                <div style={{ paddingBottom: 2, paddingRight: 4, height: '3%', paddingLeft: 4, display: 'flex', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', backgroundColor: 'blue', }}>
                    <div style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <i className="fa fa-wifi" style={{ color: 'white', fontSize: 8 }} />
                    </div>
                    <div style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <i className="fa fa-code" style={{ color: 'white', fontSize: 8 }} />
                        <i className="fa fa-battery-1" style={{ color: 'white', fontSize: 8, marginLeft: 4 }} />
                    </div>
                </div>
                <Scrollbars autoHide style={{ height: '99%' }} autoHideTimeout={500}>
                    {this.renderLayout(this.state.ScreenChosen, this.state.layoutChosen)}
                </Scrollbars>
            </div>
        );
    }
}
