import React from 'react';
import './DataBase.css';
import '../../GlobalStyleSheet.css';
import { Scrollbars } from 'react-custom-scrollbars';
import * as appData from '../../variableData'
var userApp_JSON = localStorage.getItem('JSON');
var AppJson = JSON.parse(userApp_JSON);


const DataBase = () => (

    <div className='MainContainer'>
        <div className="UpperContainer">
            <div className="Heading"> <h2>Add DataBase</h2></div>
        </div>
        <Scrollbars style={{ height: '420px' }} >
            <div className="lowerContainer">
                <Content />
            </div></Scrollbars>
    </div>

);


class Content extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
        };
    }
    componentDidMount() {
        console.log("componentdidmount AppJson");
        // console.log(AppJson);
    }
    AddScreenModules(ModalId) {
        // alert("hello");
        var modal = document.getElementById(ModalId);
        modal.style.display = "block";
    }
    // window.onclick = function(event) {
    //   var modal = document.getElementById('ModalId');
    //   if (event.target === modal) {
    //       modal.style.display = "none";
    //   }
    // }
    CloseScreenModules(ModalId) {
        var modal = document.getElementById(ModalId);
        modal.style.display = "none";
    }

    render() {
        return (
            <div >
                <div className="Add_Database">
                    <div className="database_button hvr-sweep-to-left" onClick={() => this.AddScreenModules("WordPressModal")}>
                        <span className="database_Name">{"WordPress  "}
                            {/* <i className="fa fa-plus " aria-hidden="true" >
                </i> */}
                        </span>
                    </div>
                </div>

                <div className="Add_Database">
                    <div className=" database_button hvr-sweep-to-left" onClick={() => this.AddScreenModules("FireBaseModal")}>
                        <span className="database_Name">{"FireBase  "}
                            {/* <i className="fa fa-plus " aria-hidden="true" >
                </i> */}
                        </span>
                    </div>
                </div>

                <div id="WordPressModal" className="modal">
                    <div className="modal_content">
                        <span className="close" onClick={() => this.CloseScreenModules("WordPressModal")}>&times;</span>
                        <div>
                            <Scrollbars style={{ height: 450, width: "100%" }}>
                                <WordPress />
                            </Scrollbars>
                        </div>
                    </div>
                </div>

                <div id="FireBaseModal" className="modal">
                    <div className="modal_content">
                        <span className="close" onClick={() => this.CloseScreenModules("FireBaseModal")}>&times;</span>
                        <div>
                            <Scrollbars style={{ height: 450, width: "100%" }}>
                                <FireBase />
                            </Scrollbars>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}
export default DataBase;

// function SaveDatabase(database) {
//     var userApp_JSON = localStorage.getItem('JSON');
//     var AppJson = JSON.parse(userApp_JSON);
//     if (database === "FireBase") {
//         var apiKey = document.getElementById("apiKey").value;
//         var authDomain = document.getElementById("authDomain").value;
//         var databaseURL = document.getElementById("databaseURL").value;
//         var projectId = document.getElementById("projectId").value;
//         var storageBucket = document.getElementById("storageBucket").value;
//         var messagingSenderId = document.getElementById("messagingSenderId").value;
//         // var data = document.getElementById("data").value;
//         if (apiKey === "" || authDomain === "" || databaseURL === "" || projectId === "" || storageBucket === "" || messagingSenderId === "") {
//             alert("Please enter details");
//         }
//         else {
//             AppJson["Database"] = {
//                 "SelectedDatabase": database,
//                 "Firebase": {
//                     "apiKey": "AIzaSyD2t3ktLCOJgL7gsXt13-pIsS45migdIsM",
//                     "authDomain": "foodie-9d47e.firebaseapp.com",
//                     "databaseURL": "https://foodie-9d47e.firebaseio.com",
//                     "projectId": "foodie-9d47e",
//                     "storageBucket": "foodie-9d47e.appspot.com",
//                     "messagingSenderId": "107751693378"
//                 }
//             }
//             console.log(AppJson);
//             document.getElementById("apiKey").value = "";
//             document.getElementById("authDomain").value = "";
//             document.getElementById("databaseURL").value = "";
//             document.getElementById("projectId").value = "";
//             document.getElementById("storageBucket").value = "";
//             document.getElementById("messagingSenderId").value = "";
//         }
//     }

//     localStorage.setItem('JSON', JSON.stringify(AppJson));
// }
class WordPress extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
        };
    }
    SaveDatabase() {
        var userApp_JSON = localStorage.getItem('JSON');
        var AppJson = JSON.parse(userApp_JSON);
            if (this.state.URL ) {
                alert("Please enter URL");
            }
            else {
                AppJson["Database"] = {
                    "SelectedDatabase": "Wordpress",
                    "Wordpress": {
                        // "url": this.state.URL
                        "url": "https://prashant-pandeyme.000webhostapp.com"
                    }
                }
                console.log(AppJson);
               document.getElementById("URL").value = "";
            }
            localStorage.setItem('JSON', JSON.stringify(AppJson));
        }
        handleChange(e) {    
            var varname=e.target.name;
            var value = e.target.value;  
            this.setState({[varname]:value});    
        }
    render() {
        return (
            <div className='MainContainer'>
                <div className="UpperContainer">
                    <div className="Heading">
                        <h2>WordPress</h2>
                    </div>
                </div>
                <div className="Input_Container">
                    <div className="Input_div">
                        <label className="Input_Label" id="1">
                            Enter WordPress URL:
                       <input name="URL" type="text" onChange={(event)=>this.handleChange(event)} className="url_textbox" autoFocus id="URL" />
                        </label>
                    </div>     
        
                    <div className="Input_div">
                        <button className="Save_Button" onClick={() => this.SaveDatabase()}>Save</button>
                    </div>
        
                </div>
        
            </div>
        );
    }
}

class FireBase extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data:{
                "apiKey":"",
                "authDomain":"",
                "databaseURL":"",
                "projectId":"",
                "storageBucket":"",
                "messagingSenderId":"",
            }
          
        };
    }
    SaveDatabase() {
        console.log(this.state.apiKey);
        var userApp_JSON = localStorage.getItem('JSON');
        var AppJson = JSON.parse(userApp_JSON);     
               
            if (this.state.apiKey || this.state.authDomain || this.state.databaseURL || this.state.projectId || this.state.storageBucket || this.state.messagingSenderId ) {
                alert("Please enter details");
            }
            else {
                AppJson["Database"] = {
                    "SelectedDatabase": "FireBase",
                    "Firebase": {
                        "apiKey": "AIzaSyD2t3ktLCOJgL7gsXt13-pIsS45migdIsM",
                        "authDomain": "foodie-9d47e.firebaseapp.com",
                        "databaseURL": "https://foodie-9d47e.firebaseio.com",
                        "projectId": "foodie-9d47e",
                        "storageBucket": "foodie-9d47e.appspot.com",
                        "messagingSenderId": "107751693378"
                        // "apiKey": this.state.apiKey,
                        // "authDomain": this.state.authDomain,
                        // "databaseURL":this.state.databaseURL,
                        // "projectId": this.state.projectId,
                        // "storageBucket": this.state.storageBucket,
                        // "messagingSenderId": this.state.messagingSenderId
                    }
                }
                console.log(AppJson);            
            }
    
        localStorage.setItem('JSON', JSON.stringify(AppJson));
    }
    handleChange(e) {    
        var varname=e.target.name;
        var value = e.target.value;  
        this.setState({[varname]:value});    
    }
    render() {
        return (

            <div className='MainContainer'>
                <div className="UpperContainer">
                    <div className="Heading"><h2>FireBase</h2></div>
                </div>
                <div className="Input_Container">
                    <div className="Input_div">
                        <label className="Input_Label" id="1">
                            Enter apiKey:
                       <input name="apiKey" type="text" onChange={(event)=>this.handleChange(event)} className="url_textbox" autoFocus id="apiKey" />
                        </label>
                    </div>
        
                    <div className="Input_div">
                        <label className="Input_Label" id="1">
                            Enter authDomain:
                    <input name="authDomain" type="text" onChange={(event)=>this.handleChange(event)} className="url_textbox" id="authDomain" />
                        </label>
                    </div>
        
                    <div className="Input_div">
                        <label className="Input_Label" id="1">
                            Enter databaseURL:
                    <input name="databaseURL" type="text" onChange={(event)=>this.handleChange(event)} className="url_textbox" id="databaseURL" />
                        </label>
                    </div>
        
                    <div className="Input_div">
                        <label className="Input_Label" id="1">
                            Enter projectId:
                    <input name="projectId" type="text" onChange={(event)=>this.handleChange(event)} className="url_textbox" id="projectId" />
                        </label>
                    </div>
        
                    <div className="Input_div">
                        <label className="Input_Label" id="1">
                            Enter storageBucket:
                    <input name="storageBucket" type="text" onChange={(event)=>this.handleChange(event)} className="url_textbox" id="storageBucket" />
                        </label>
                    </div>
        
                    <div className="Input_div">
                        <label className="Input_Label" id="1">
                            Enter messagingSenderId:
                    <input name="messagingSenderId" type="text" onChange={(event)=>this.handleChange(event)} className="url_textbox" id="messagingSenderId" />
                        </label>
                    </div>
        
                    <div className="Input_div">
                        <button className="Save_Button" onClick={() => this.SaveDatabase()}>Save</button>
                    </div>
        
                </div>
            </div>
        );
    }
}
