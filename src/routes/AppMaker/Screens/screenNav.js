import React from 'react'
import { BrowserRouter as Router, Route, NavLink, Switch, } from 'react-router-dom';
import '../GlobalStyleSheet.css';
import  NavigationScreen  from './Navigation/Navigation';
import { LoginScreen } from './Login/Login';
import SignUpScreen from './SignUp/SignUp';
import NewsFeed from './NewsFeed/NewsFeed';
import NewsDescription from './NewsDescription/NewsDescription';
import Modules from "../preview/Modules/Modules.js"
import { Scrollbars } from 'react-custom-scrollbars';
import * as appData from '../variableData';
// var previewScreenName = appData.previewScreen;

var userApp_JSON = localStorage.getItem('JSON');
var AppJson = JSON.parse(userApp_JSON);

const screenRoutes = [
  {
    path: '/News_App/screen/Navigation/',
    exact: true,
    sidebar: () => <div>Navigation!</div>,
    main: () => <NavigationScreen />
  },
  {
    path: '/News_App/screen/LoginScreen/',
    exact: true,
    sidebar: () => <div>Login!</div>,
    main: () => <LoginScreen />
  },
  {
    path: '/News_App/screen/SignUpScreen',
    exact: true,
    sidebar: () => <div>SignUp!</div>,
    main: () => <SignUpScreen />
  },
  {
    path: '/News_App/screen/NewsFeed',
    exact: true,
    sidebar: () => <div>NewsFeed!</div>,
    main: () => <NewsFeed />
  },
  {
    path: '/News_App/screen/NewsDescription',
    exact: true,
    sidebar: () => <div>NewsDescription!</div>,
    main: () => <NewsDescription />
  }
]
function previewscreen(ScreenName, layout) {
  appData.setPreview(ScreenName, layout);
  console.log("screen selected in preview screen function");
  console.log(appData.previewScreen);
  console.log(appData.previewLayout);
}
function AddScreenModules() {
  // alert("hello");
  var modal = document.getElementById('myModal');
  modal.style.display = "block";
}
// window.onclick = function(event) {
//   var modal = document.getElementById('myModal');
//   if (event.target === modal) {
//       modal.style.display = "none";
//   }
// }
export function CloseScreenModules() {
  var modal = document.getElementById('myModal');
  modal.style.display = "none";
}

export default class ScreenNavBar  extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      ScreenList: [],
    }

  }
  render() {
    return (
  <Router>
    <div className='Nav_MainContainer'>
      <div className='Nav_left '>

        <div className='Nav_heading'>
          <div style={{ flex: 6 }}>
            <h4>Screens</h4></div>
          <div className="AddScreen_icon">
            <i className="fa fa-plus" aria-hidden="true" onClick={() => AddScreenModules()}></i>
          </div>
        </div>
     
        {
          AppJson.Screens.map((Screen,index)=>
        (<NavLink key={index} className='Nav_linkscreen' to={Screen.Path} onClick={() => previewscreen(Screen.Name, "Layout1")}>
        <div className='Nav_List'>
          <div className='Nav_Item' ><i className="fa fa-square-o" aria-hidden="true"></i>
            <span> {Screen.Name}</span>
          </div>
        </div>
        </NavLink>)
    )}

        {/* <NavLink className='Nav_linkscreen' to="/News_App/screen/login/" onClick={() => previewscreen("LoginScreenView", "Layout1")}>
          <div className='Nav_List'>
            <div className='Nav_Item' ><i className="fa fa-square-o" aria-hidden="true"></i>
              <span> login</span>
            </div>
          </div>
        </NavLink>

        <NavLink className='Nav_linkscreen' to="/News_App/screen/SignUp" onClick={() => previewscreen("SignUpScreenView", "Layout1")}>
          <div className='Nav_List'>
            <div className='Nav_Item'><i className="fa fa-square-o" aria-hidden="true"></i>
              <span> SignUp</span>
            </div>
          </div>
        </NavLink>


        <NavLink className='Nav_linkscreen' to="/News_App/screen/NewsFeed"
          activeClassName="activeLink" onClick={() => previewscreen("NewsFeed", "Layout1")}>
          <div className='Nav_List' >
            <div className='Nav_Item'>
              <i className="fa fa-square-o" aria-hidden="true"></i>
              <span> NewsFeed</span>
            </div></div>
        </NavLink>

        <NavLink className='Nav_linkscreen' to="/News_App/screen/NewsDescription" onClick={() => previewscreen("NewsDescription", "Layout1")}>
          <div className='Nav_List' activeClassName="active">
            <div className='Nav_Item'><i className="fa fa-square-o" aria-hidden="true"></i>
              <span>  News Description</span>
            </div>
          </div>
        </NavLink> */}

      </div>
      <div className='Nav_right '>
      <div id="myModal" className="modal">
          <div className="modal_content">
            <span className="close" onClick={() => CloseScreenModules()}>&times;</span>
            <span style={{ fontSize: 25 }}>Add Module</span>
            <div>
              <Scrollbars style={{ height: 450, width: "100%" }}>
                <Modules />
              </Scrollbars>
            </div>
          </div>
        </div>
        <Switch>
          {screenRoutes.map((route, index) => (

            <Route
              key={index}
              path={route.path}
              exact={route.exact}
              component={route.main}
            />
          ))}
        </Switch>
      </div>
    </div>
  </Router>
)}
}