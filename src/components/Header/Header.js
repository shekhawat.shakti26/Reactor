import React from 'react';
import {Link} from 'react-router-dom';
import './Header.css';
import logoUrl from './logo-small.png';
import logoUrl2x from './logo-small@2x.png';
// import YoustartLabs from './YoustartLabs.png';
// import Logo from './Logo_Youstart-white.png';
import Navigation from '../Navigation/Navigation';

class Header extends React.Component {
  render() {
    return (
      <div className="Header-root">
        <div className="Header-container">
          <Navigation />
          <Link className="Header-brand" to="/">
            <img
              src={logoUrl}
              srcSet={`${logoUrl2x} 2x`}
              width="38"
              height="38"
              alt="React"
            />
            <span className="Header-brandTxt">Reactor</span>
          </Link>
        </div>
      </div>
    );
  }
}

export default Header;