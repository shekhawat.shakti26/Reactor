import React, { Component } from 'react'
import {
  BrowserRouter as Router,
  Route,
  NavLink,
} from 'react-router-dom'
import './Photos.css';
import '../../GlobalStyleSheet.css';
import { Scrollbars } from 'react-custom-scrollbars';
import firebaseApp from "../../../../FireBase";
// import * as appData from '../../variableData';
import { BarLoader } from 'react-spinners';
// var userApp_JSON = localStorage.getItem('JSON');
// var AppJson = JSON.parse(userApp_JSON);
// var storageRef = firebaseApp.storage().ref();


const PhotosRoutes = [
  {
    path: '/News_App/styles/Photos/',
    exact: true,
    sidebar: () => <div>content!</div>,
    main: () => <IconUpload />
  },
  {
    path: '/News_App/styles/Photos/splash',
    exact: true,
    sidebar: () => <div>layout!</div>,
    main: () => <SplashUpload />
  },
]

const Photos = () => (
  <Router>
    <div className='MainContainer'>
      <div className="UpperContainer jumbotron jumbotron-fluid">
        <div className="Heading"> <h2>Photos</h2></div>
        <div className="Photos_TabsDiv">
          <NavLink style={{ textDecoration: 'none' }} activeClassName="photoslinksActive" className='photoslinks' to="/News_App/styles/Photos/">
            <kbd className="Photos_TabsDivText">ICON</kbd>
          </NavLink>
          <NavLink style={{ textDecoration: 'none' }} activeClassName="photoslinksActive" className='photoslinks' to="/News_App/styles/Photos/splash">
            <kbd className="Photos_TabsDivText">SPLASH</kbd>
          </NavLink>
        </div>
      </div>
      <div className="lowerContainer">
        <div>
          <Scrollbars style={{ height: 380 }}>
            {PhotosRoutes.map((route, index) => (

              <Route
                key={index}
                path={route.path}
                exact={route.exact}
                component={route.main}
              />
            ))}
          </Scrollbars>
        </div>
      </div>
    </div>
  </Router>
);

class SplashUpload extends Component {
  constructor(props) {
    super(props);
    this.state = {
      file: '',
      imagePreviewUrl: '',
    };
    this._handleImageChange = this._handleImageChange.bind(this);
    this._handleSubmit = this._handleSubmit.bind(this);
  }
  componentDidMount() {
    var user = firebaseApp.auth().currentUser;
    this.setState({ userData: user });
    // console.log("user");
    // console.log();
  }
  _handleSubmit(e) {
    e.preventDefault();
 if(this.state.Height >= 2208 || this.state.Width  >= 2208){
  var file = this.state.file // use the Blob or File API
  if (file) {
    var box = document.getElementById('overlay');
    box.style.display = "block";
    var storageRef = firebaseApp.storage().ref();
    var userApp_JSON = localStorage.getItem('JSON');
    var AppJson = JSON.parse(userApp_JSON);
    var userId = AppJson.userId;
    var projectId = AppJson.projectId;
    var ref = storageRef.child(userId + "/" + projectId + "/splash");
    ref.put(file).then(function (snapshot) {
      console.log(snapshot.downloadURL);
      AppJson.Splash = snapshot.downloadURL;
      console.log('splash uploaded');
      localStorage.setItem('JSON', JSON.stringify(AppJson));
      setTimeout(() => { box.style.display = "none"; }, 1000)
    });
  }
  }
  else{
    alert('image size is too small');
 }
  }
  _handleImageChange(e) {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];
    console.log(file)

    reader.onloadend = () => {
      this.setState({
        file: file,
        imagePreviewUrl: reader.result
      });
      setTimeout(() => {
        let { naturalWidth, naturalHeight } = document.getElementById("splash");
        console.log(naturalWidth, naturalHeight);
        this.setState({
          Height: naturalHeight,
          Width: naturalWidth
        });
        if (naturalHeight < 2208 || naturalWidth < 2208) {
          alert('image size is too small');

        }
      }, 10)
    }
    reader.readAsDataURL(file)
  }

  render() {
    let { imagePreviewUrl } = this.state;
    let $imagePreview = null;
    if (imagePreviewUrl) {
      $imagePreview = (
        <img id="splash" style={{ height: 480, width: 270 }} alt="..." src={imagePreviewUrl} />);
    }

    return (
      <div className="container iconMainDiv">

        <div id="overlay" className="overlay">
          <div id="loader" className="loader">
            {/* <RingLoader style={{ marginLeft: 15,height:500,width:200 }} color={'#123abc'} loading={this.state.loading} /> */}
            <BarLoader style={{ marginLeft: 15 }} color={'#123abc'} loading={this.state.loading} />
            {/* <ScaleLoader style={{ marginLeft: 15 }} color={'#123abc'} loading={this.state.loading} /> */}
            {/* <SyncLoader style={{ marginLeft: 15 }} color={'#123abc'} loading={this.state.loading} /> */}
          </div>
        </div>

        <form className="iconUploadDiv" onSubmit={this._handleSubmit}>
          <label className="custom-file">
            <input type="file" id="file" className="custom-file-input" onChange={this._handleImageChange} />
            <span className="custom-file-control"></span>
          </label>
          <button className="btn btn-info" type="submit">Upload Splash</button>
        </form>
        <div className="iconImageContainer">
          <div className="splashContainer">
            {$imagePreview}
          </div>
          <blockquote className="iconInstructions">
            <strong className="blockquote-footer display-4 font-weight-bold">"This image will be your app's splash screen."</strong>
            <p className="iconInstructionsText">Splash image file should be a square image with min resolution of 2208x2208px</p>
            <em>Please, Upload your image before leaving this section</em>
          </blockquote>
        </div>
      </div>
    )
  }
}
class IconUpload extends Component {
  constructor(props) {
    super(props);
    this.state = {
      file: '',
      imagePreviewUrl: '',
    };
    this._handleImageChange = this._handleImageChange.bind(this);
    this._handleSubmit = this._handleSubmit.bind(this);
  }
  componentDidMount() {
    var user = firebaseApp.auth().currentUser;
    this.setState({ userData: user });
    // console.log("user");
    // console.log();
  }

  _handleSubmit(e) {
    e.preventDefault();
    var file = this.state.file // use the Blob or File API
    if (file) {
      var box = document.getElementById('overlay');
      box.style.display = "block";
      var storageRef = firebaseApp.storage().ref();
      var userApp_JSON = localStorage.getItem('JSON');
      var AppJson = JSON.parse(userApp_JSON);
      var userId = AppJson.userId;
      var projectId = AppJson.projectId;
      var ref = storageRef.child(userId + "/" + projectId + "/Icon");
      ref.put(file).then(function (snapshot) {
        console.log(snapshot.downloadURL);
        AppJson.Icon = snapshot.downloadURL;
        console.log(AppJson);
        console.log('Icon uploaded');
        localStorage.setItem('JSON', JSON.stringify(AppJson));
        setTimeout(() => { box.style.display = "none"; }, 1000)
      });
    }
  }

  _handleImageChange(e) {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];
    // console.log(width)

    reader.onloadend = () => {
      this.setState({
        file: file,
        imagePreviewUrl: reader.result
      });
      setTimeout(() => {
        let { naturalWidth, naturalHeight } = document.getElementById("Icon");
        console.log(naturalWidth, naturalHeight);
        if (naturalHeight < 1024) {

        }
      }, 10)
    }
    reader.readAsDataURL(file)
  }

  render() {
    let { imagePreviewUrl } = this.state;
    let $imagePreview = null;
    if (imagePreviewUrl) {
      $imagePreview = (
        <img id="Icon" style={{ height: 192, width: 192 }} alt="..." src={imagePreviewUrl} />);
    }

    return (
      <div className="container iconMainDiv">
        <form className="iconUploadDiv" onSubmit={this._handleSubmit}>
          <label className="custom-file">
            <input type="file" id="file" className="custom-file-input" onChange={this._handleImageChange} />
            <span className="custom-file-control"></span>
          </label>
          <button className="btn btn-info" type="submit" >Upload Icon</button>
        </form>
        <div className="iconImageContainer">
          <div className="iconContainer">
            {$imagePreview}
          </div>
          <blockquote className="iconInstructions">
            <strong className="blockquote-footer display-4 font-weight-bold">"This image will be your app icon."</strong>
            <p className="iconInstructionsText">Icon image file should be a square image with min resolution of 192x192px</p>
            <em>Please, Upload your image before leaving this section</em>
          </blockquote>
        </div>
      </div>
    )
  }

}

export default Photos