import React from 'react'
import { BrowserRouter as Router, Route, Link, } from 'react-router-dom';
// import './Login.css';
import '../../GlobalStyleSheet.css';
import { Scrollbars } from 'react-custom-scrollbars';
import * as appData from '../../variableData';
var userApp_JSON = localStorage.getItem('JSON');
var AppJson = JSON.parse(userApp_JSON);

const LoginRoutes = [
    {
        path: '/News_App/screen/LoginScreen/Content',
        exact: true,
        sidebar: () => <div>content!</div>,
        main: () => <Content />
    },
    {
        path: '/News_App/screen/LoginScreen/layout',
        sidebar: () => <div>layout!</div>,
        main: () => <Layout />
    },
]

export class LoginScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }
    render() {
        return (
            <Router>
                <div className='MainContainer'>
                    <div className="UpperContainer">
                        <div className="Heading"> <h2>Login</h2></div>
                        <div className="TabsDiv">
                            <Link className='links' to="/News_App/screen/LoginScreen/Content"> Content</Link>
                            <Link className='links' to="/News_App/screen/LoginScreen/layout"> Layout</Link>
                        </div>
                    </div>
                    <Scrollbars style={{ height: 410 }} >
                        <div className="lowerContainer">
                            {LoginRoutes.map((route, index) => (

                                <Route
                                    key={index}
                                    path={route.path}
                                    exact={route.exact}
                                    component={route.main}
                                />
                            ))}
                        </div></Scrollbars>
                </div>
            </Router>
        );
    }
}
function selectLayout(layout, Layout_Id) {
    var userApp_JSON = localStorage.getItem('JSON');
    var AppJson = JSON.parse(userApp_JSON);
    var x = document.getElementById(Layout_Id);
    var y;
    if (layout === 'Layout1') {
        y = document.getElementById("Phone2");
        x.style.backgroundColor = "Black";
        y.style.backgroundColor = "grey";
        AppJson["LoginModule"].Layout = layout;
        appData.setPreview("Login", layout);
        //  alert('layout1');
        // selectedlayout:layout;
    }
    if (layout === 'Layout2') {
        y = document.getElementById("Phone1");
        x.style.backgroundColor = "Black";
        y.style.backgroundColor = "grey";
        AppJson["LoginModule"].Layout = layout;
        appData.setPreview("Login", layout);
        // alert('layout2');
        // selectedlayout:layout;
    }
    localStorage.setItem('JSON', JSON.stringify(AppJson));
    console.log("newJson");
    console.log(AppJson);
    console.log(appData.previewScreen);
    console.log(appData.previewLayout);
}

const Layout = () => (
    <div>

        <div className="Products_screen_layout_div" onClick={() => selectLayout("Layout1", "Phone1")}>
            <div className="Products_screen_div" id="Phone1">
                <img className="Products_screen-image" alt="..." src="https://extensions.static.shoutem.com/shoutem/events/1.4.1/server/assets/images/tile-list.png" />
            </div>
            <div className="Products_screen_name">Tile list</div>
        </div>

        <div className="Products_screen_layout_div" onClick={() => selectLayout("Layout2", "Phone2")}>
            <div className="Products_screen_div" id="Phone2">
                <img className="Products_screen-image" alt="..." src="https://extensions.static.shoutem.com/shoutem/events/1.4.1/server/assets/images/compact-list.png" />
            </div>
            <div className="Products_screen_name">Compact list</div>
        </div>
        {/* <div className="Products_screen_layout_div ">
            <div className="Products_screen_div">
                <img className="Products_screen-image" src="https://extensions.static.shoutem.com/shoutem/about/1.4.1/server/assets/images/large-photo-clear-navbar.png" />
            </div>
            <div className="Products_screen_name">Large photo with transparent navbar</div>
        </div>

        <div className="Products_screen_layout_div">
            <div className="Products_screen_div">
                <img className="Products_screen-image" src="https://extensions.static.shoutem.com/shoutem/about/1.4.1/server/assets/images/medium-photo-clear-navbar.png" />
            </div>
            <div className="Products_screen_name">Medium photo with transparent navbar</div>
        </div>

        <div className="Products_screen_layout_div">
            <div className="Products_screen_div">
                <img className="Products_screen-image" src="https://extensions.static.shoutem.com/shoutem/about/1.4.1/server/assets/images/large-photo-solid-navbar.png" />
            </div>
            <div className="Products_screen_name">Large photo with solid navbar</div>
        </div>

        <div className="Products_screen_layout_div">
            <div className="Products_screen_div">
                <img className="Products_screen-image" src="https://extensions.static.shoutem.com/shoutem/about/1.4.1/server/assets/images/medium-photo-solid-navbar.png" />
            </div>
            <div className="Products_screen_name">Medium photo with solid navbar</div>
        </div> */}
    </div>
);


export class Content extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            Title: AppJson["LoginModule"]["LoginScreen"]["Title"],
            Textbox_Email: AppJson["LoginModule"]["LoginScreen"]["components"]["Textbox_Email"],
            Textbox_Password: AppJson["LoginModule"]["LoginScreen"]["components"]["Textbox_Password"],
            Button: AppJson["LoginModule"]["LoginScreen"]["components"]["Button"],
            Bottom_Text: AppJson["LoginModule"]["LoginScreen"]["components"]["Bottom_Text"],
            Bottom_Link: AppJson["LoginModule"]["LoginScreen"]["components"]["Bottom_Link"],
        }
    }

    handleChange(e) {
        var userApp_JSON = localStorage.getItem('JSON');
        var AppJson = JSON.parse(userApp_JSON);
        var componentType = e.target.name;
        var text = e.target.value;
        // console.log(componentType+ ": "+text)
        this.setState({ [componentType]: text });

        if (text) {
            if (componentType === "Title") {
                AppJson["LoginModule"]["LoginScreen"]["Title"] = text;
                console.log(AppJson);
            }
            else {
                AppJson["LoginModule"]["LoginScreen"]["components"][componentType] = text;
                console.log(AppJson);
            }
        }
        localStorage.setItem('JSON', JSON.stringify(AppJson));
    }

    // Display_Hide(component_id) {
    //     var x = document.getElementById(component_id);
    //     if (x.style.display === "none") {
    //         x.style.display = "block";
    //     } else {
    //         x.style.display = "none";
    //     }
    // }
    render() {
        return (<div>
            <div className="heading" >
                <h2></h2></div>
            <div className="Content">
                    <div className="Content_left">
                        <label>
                            {/* <input onChange={() => this.Display_Hide("0")}
                                name="TEXTBOX"
                                type="checkbox"
                                defaultChecked /> */}
                                TITLE TEXT</label>
                    </div>
                    <div className="Content_right">
                        <label style={{ display: "block" }} id="0">

                            <input onChange={(event) => this.handleChange(event)}
                                value={this.state.Title}
                                name="Title" type="text" id="0_textbox" />
                        </label>
                    </div>
                </div>

            <div className="Content">
                <div className="Content_left">
                    <label>
                        {/* <input onChange={() => this.Display_Hide("1")}
                            name="TEXTBOX"
                            type="checkbox"
                            defaultChecked/> */}
                            Email</label>
                </div>
                <div className="Content_right">
                    <label style={{ display: "block" }} id="1">

                        <input onChange={(event) => this.handleChange(event)}
                            value={this.state.Textbox_Email}
                            name="Textbox_Email" type="text" id="1_textbox" />
                    </label>
                </div>
            </div>

            <div className="Content">
                <div className="Content_left">
                    <label>
                        {/* <input onChange={() => this.Display_Hide("2")}
                            name="TEXTBOX"
                            type="checkbox"
                            defaultChecked /> */}
                        PASSWORD
    </label>
                </div>
                <div className="Content_right">
                    <label style={{ display: "block" }} id="2">

                        <input onChange={(event) => this.handleChange(event)}
                            value={this.state.Textbox_Password}
                            name="Textbox_Password" type="text" id="2_textbox" />
                    </label>
                </div>
            </div>

            <div className="Content">
                <div className="Content_left">
                    <label>
                        {/* <input onChange={() => this.Display_Hide("3")}
                         name="BUTTON" type="checkbox" defaultChecked /> */}
                        SIGN  IN
        </label>
                </div>
                <div className="Content_right">
                    <label style={{ display: "block" }} id="3">

                        <input onChange={(event) => this.handleChange(event)}
                            value={this.state.Button}
                            name="Button" type="text" id="3_textbox" />
                    </label>
                </div>
            </div>

            <div className="Content">
                <div className="Content_left">
                    <label>
                        {/* <input onChange={() => this.Display_Hide("5")} 
                        name="BUTTON" type="checkbox" defaultChecked /> */}
                        Bottom Text
        </label>
                </div>
                <div className="Content_right">
                    <label style={{ display: "block" }} id="5">

                        <input onChange={(event) => this.handleChange(event)}
                            value={this.state.Bottom_Text}
                            name="Bottom_Text" type="text" id="5_textbox" />
                    </label>
                </div>
            </div>

            <div className="Content">
                <div className="Content_left">
                    <label>
                        {/* <input onChange={() => this.Display_Hide("6")}
                         name="BUTTON" type="checkbox" defaultChecked /> */}
                        Bottom Link
        </label>
                </div>
                <div className="Content_right">
                    <label style={{ display: "block" }} id="6">

                        <input onChange={(event) => this.handleChange(event)}
                            value={this.state.Bottom_Link}
                            name="Bottom_Link" type="text" id="6_textbox" />
                    </label>
                </div>
            </div>
        </div>
        );
    }
}