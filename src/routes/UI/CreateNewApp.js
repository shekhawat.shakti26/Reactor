import React from 'react';
import './CreateNewApp.css'
// import Footer from '../../components/Footer/Footer'
import Header from '../../components/Header';
import { Scrollbars } from 'react-custom-scrollbars';
import firebaseApp from "../../FireBase";
import * as appData from '../AppMaker/variableData';
import { SyncLoader} from 'react-spinners';

var AppJson = appData.makeappJson;
var userID = localStorage.getItem('UserId');


export default class CreateNewApp extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      AppList: [],
      loading: true
    }

  }
  componentDidMount() { // on page load call the function(listenforitems) to get list of all the Apps
    var box= document.getElementById('overlay');
    box.style.display = "block";
    // var userID = localStorage.getItem('UserId');
    console.log(userID)
    this.AppsRef = firebaseApp.database().ref().child('Projects/' + userID);
    this.listenForItems(this.AppsRef);
  }

  listenForItems(AppsRef) { //get the list of all the users signuped
    var box= document.getElementById('overlay');
    // AppsRef.on('value', (snap) => {
    //   var User_AppList = [];
    //   snap.forEach((child) => {
    //     // console.log(child.val())
    //     User_AppList.push({
    //       AppName: child.val().Name,
    //       ProjectId: child.val().ProjectId,
    //     });
    //   });
    //   this.setState({ AppList: User_AppList })
      box.style.display = "none";
    // });

  }

  AddScreenModules() {
    var modal = document.getElementById("NewAppModal");
    modal.style.display = "block";
  }

  CloseScreenModules() {
    var modal = document.getElementById("NewAppModal");
    modal.style.display = "none";
  }

  async CreateNewApp() {
    var userID = localStorage.getItem('UserId');
    var date = new Date().toString();
    var modal = document.getElementById("NewAppModal");
    var name = document.getElementById("NewAppName").value;
    var id = name + date.substring(8, 24);
    var newid = id.replace(/\s/g, "");
    var projectId = newid.replace(/:/g, "");
    AppJson.userId = userID;
    AppJson.appName = name;
    AppJson.projectId = projectId;
    console.log(AppJson);
    firebaseApp.database().ref('Projects/' + userID + "/" + projectId).set({ //store messages in database
      Name: name,
      ProjectId: projectId,
      JSON: JSON.stringify(AppJson),
      BuildApk: "null"
    }).then(() => {
      modal.style.display = "none";
    });
  }

  GetAppData(ProjectId) {    
    var userID = localStorage.getItem('UserId');
    var AppRef = firebaseApp.database().ref('Projects/' + userID + "/" + ProjectId);
    var App_Data = [];
    AppRef.on('value', (snap) => {
      snap.forEach((child) => {
        App_Data.push({
          data: child.val()
        });
      });
    });
    
    // console.log(App_Data);
    // console.log(App_Data[1].data);
   localStorage.setItem('JSON', App_Data[1].data);
    window.location = '/News_App/'

  }
  render() {
    return (
      <div className="CreateApp_Root">
        <Header />
        <div className="CreateApp_Container">

        <div id="overlay" className="overlay">
            <div id="loader" className="loader">
            {/* <RingLoader style={{ marginLeft: 15,height:500,width:200 }} color={'#123abc'} loading={this.state.loading} /> */}
            {/* <BarLoader style={{ marginLeft: 15 }} color={'#123abc'} loading={this.state.loading} /> */}
            {/* <ScaleLoader style={{ marginLeft: 15 }} color={'#123abc'} loading={this.state.loading} /> */}
            <SyncLoader style={{ marginLeft: 15 }} color={'#123abc'} loading={this.state.loading} />
              </div>
                </div>

          <div className="CreateApp_Content">
            <Scrollbars>
              <div className="row">
                {this.state.AppList.map((item ,index) => (
                  <div key={index} className="CreateApp_Items col-md-4" onClick={() => this.GetAppData(item.ProjectId)}>
                    <span className="Items_Text">{item.AppName}</span>
                  </div>
                ))}

                <div className="col-lg-3 col-md-2 col-sm-2 CreateApp_Items" onClick={() => this.AddScreenModules()}>
                  <span className="Items_Text">Add New +</span>
                </div>
              </div>
            </Scrollbars>
          </div>
        

          <div id="NewAppModal" className="modal">
            <div className="modal_content NewAppModal_content ">
              <span className="close" onClick={() => this.CloseScreenModules()}>&times;</span>
              <div className="Input_Container">
                <div className="Input_div">
                  <label className="Input_Label" id="1">
                    App Name:
                 <input name="URL" type="text" className="url_textbox" autoFocus id="NewAppName" />
                  </label>
                </div>

                <div className="Input_div">
                  <button className="Save_Button" onClick={() => this.CreateNewApp()}>Create</button>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    );
  }
}
