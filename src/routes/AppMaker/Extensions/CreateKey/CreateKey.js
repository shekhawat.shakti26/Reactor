import React from 'react';
import './CreateKey.css';
import '../../GlobalStyleSheet.css';
import { Scrollbars } from 'react-custom-scrollbars';
import * as appData from '../../variableData';
import firebaseApp from '../../../../FireBase';
import { ScaleLoader } from 'react-spinners';
var userApp_JSON = localStorage.getItem('JSON');
var AppJson = JSON.parse(userApp_JSON);


const CreateKey = () => (

    <div className='MainContainer'>
        <div className="UpperContainer">
            <div className="Heading"> <h2>Generate signing key</h2></div>
        </div>
        <Scrollbars style={{ height: '420px' }} >
            <div className="lowerContainer">
                <Content />
            </div></Scrollbars>
    </div>

);


class Content extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
        };
    }
    componentDidMount() {
        console.log("componentdidmount AppJson");
        // console.log(AppJson);
    }
    GenerateKey() {
        var box = document.getElementById('overlay');
        var userId = AppJson.userId;
        var projectId = AppJson.projectId;
        var Name = document.getElementById("FL_LN").value;
        var Organization_Unit = document.getElementById("OU").value;
        var Organization = document.getElementById("O").value;
        var city = document.getElementById("CL").value;
        var State = document.getElementById("SP").value;
        var CountryCode = document.getElementById("CO").value;
        var password1 = document.getElementById("KP").value;
        var Confirmpassword1 = document.getElementById("CKP").value;
        var password2 = document.getElementById("MKAP").value;

        //  var Name  = "shakti singh";
        // var Organization_Unit  = "Youstart";
        // var Organization  = "Youstart";
        // var city  = "jaipur";
        // var State  = "rajasthan";
        // var CountryCode  = "91";
        // var password1 = "shakti";
        // var Confirmpassword1 = "shakti";
        // var password2 = "shakti";

        if (Name === "" || Organization_Unit === "" || Organization === "" || city === "" || State === ""
            || CountryCode === "" || password1 === "" || password2 === "") {
            alert("Please enter details");
        }
        else if (password1 === Confirmpassword1) {
            box.style.display = "block";
            var KeyApiJSON = {
                "firstAndLastName": Name,
                "organisationalUnit": Organization_Unit,
                "organisation": Organization,
                "city": city,
                "state": State,
                "countryCode": CountryCode,
                "keystore_password": password1,
                "key_password": password2,
                "userId": userId,
                "projectId": projectId,
            }
            console.log(KeyApiJSON);
            fetch("http://ec2-52-34-99-28.us-west-2.compute.amazonaws.com:8089/make_key", {
                method: 'POST',
                mode: 'CORS',
                body: JSON.stringify(KeyApiJSON),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
                .then((response) => response.json())
                .then(responseJson => {
                    console.log(responseJson.message);
                    if (responseJson.message === "key uploaded") {
                        this.GetUrl(password1, password2);
                        document.getElementById("FL_LN").value = "";
                        document.getElementById("OU").value = "";
                        document.getElementById("O").value = "";
                        document.getElementById("CL").value = "";
                        document.getElementById("SP").value = "";
                        document.getElementById("CO").value = "";
                        document.getElementById("KP").value = "";
                        document.getElementById("CKP").value = "";
                        document.getElementById("MKAP").value = "";

                    }

                })
                .catch(err => {
                    box.style.display = "none";
                    console.log(err);
                    alert(err)
                });

        } else {
            alert("Password do not match");
        }
    }
    GetUrl(password1, password2) {
        var box = document.getElementById('overlay');
        var userApp_JSON = localStorage.getItem('JSON');
        var AppJson = JSON.parse(userApp_JSON);
        console.log("getUrl");
        var userId = AppJson.userId;
        var projectId = AppJson.projectId;
        var storageRef = firebaseApp.storage().ref();
        storageRef.child(userId + "/" + projectId + "/my-release-key.keystore").getDownloadURL()
            .then(function (url) {
                console.log(url);
                AppJson["key"] = {
                    "url": url,
                    "password1": password1,
                    "password2": password2
                }
                console.log("saving key");
                console.log(AppJson);
                localStorage.setItem('JSON', JSON.stringify(AppJson));
                box.style.display = "none";
            })
            .catch(function (error) {
                box.style.display = "none";
                console.log(error);
            });

    }

    render() {
        return (
            <div >
                <div className="CreateKey_Input_Container">

                    <div id="overlay" className="overlay">
                        <div id="loader" className="loader">
                            {/* <RingLoader style={{ marginLeft: 15,height:500,width:200 }} color={'#123abc'} loading={this.state.loading} /> */}
                            {/* <BarLoader style={{ marginLeft: 15 }} color={'#123abc'} loading={this.state.loading} /> */}
                            <ScaleLoader style={{ marginLeft: 15 }} color={'#123abc'} loading={this.state.loading} />
                            {/* <SyncLoader style={{ marginLeft: 15 }} color={'#123abc'} loading={this.state.loading} /> */}
                        </div>
                    </div>

                    <div className="CreateKey_Input_div">
                        <label className="CreateKey_Input_Label">
                            What is your first and last name?
               <input name="URL" type="text" className="CreateKey_url_textbox" autoFocus id="FL_LN" />
                        </label>
                    </div>

                    <div className="CreateKey_Input_div">
                        <label className="CreateKey_Input_Label" >
                            What is the name of your organizational unit?
               <input name="URL" type="text" className="CreateKey_url_textbox" id="OU" />
                        </label>
                    </div>

                    <div className="CreateKey_Input_div">
                        <label className="CreateKey_Input_Label" >
                            What is the name of your organization?
            <input name="URL" type="text" className="CreateKey_url_textbox" id="O" />
                        </label>
                    </div>

                    <div className="CreateKey_Input_div">
                        <label className="CreateKey_Input_Label" >
                            What is the name of your City or Locality?
               <input name="URL" type="text" className="CreateKey_url_textbox" id="CL" />
                        </label>
                    </div>

                    <div className="CreateKey_Input_div">
                        <label className="CreateKey_Input_Label" >
                            What is the name of your State or Province?
               <input name="URL" type="text" className="CreateKey_url_textbox" id="SP" />
                        </label>
                    </div>

                    <div className="CreateKey_Input_div">
                        <label className="CreateKey_Input_Label" >
                            What is the two-letter country code for this unit?
            <input name="URL" type="text" className="CreateKey_url_textbox" id="CO" />
                        </label>
                    </div>

                    <div className="CreateKey_Input_div">
                        <label className="CreateKey_Input_Label" >
                            Enter keystore password: 
               <input name="URL" type="text" className="CreateKey_url_textbox" id="KP" />
                        </label>
                    </div>

                    <div className="CreateKey_Input_div">
                        <label className="CreateKey_Input_Label" >
                            Re-enter new password: 
            <input name="URL" type="text" className="CreateKey_url_textbox" id="CKP" />
                        </label>
                    </div>

                    <div className="CreateKey_Input_div">
                        <label className="CreateKey_Input_Label" >
                            Enter key password for "my-key-alias"
               <input name="URL" type="text" className="CreateKey_url_textbox" id="MKAP" />
                        </label>
                    </div>


                    <div className="CreateKey_Input_div">
                        {/* <input  name="URL" type="button" value="" className="Save_Button" />  */}
                        <button className="CreateKey_Save_Button" onClick={() => this.GenerateKey()}>Create</button>
                    </div>

                </div>
            </div>
        );
    }
}
export default CreateKey;