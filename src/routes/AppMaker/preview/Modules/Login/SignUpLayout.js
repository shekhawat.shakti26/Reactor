import React from 'react';
import './SignUpLayout.css';
import './LoginLayout.css';
// import * as appData from '../../../variableData';

var userApp_JSON = localStorage.getItem('JSON');
var AppJson = JSON.parse(userApp_JSON);
var data = AppJson;

export class Layout1 extends React.Component {

  render() {
    return (
      <div className="productPreview_Loginscreen">

        <div className="Loginscreen_top">
          <img className="Loginscreen_logo" src={require("../../../../../data/News App/logo.jpg")} alt="" />
          <span className="Loginscreen_Main_Heading">{data["LoginModule"]["SignUpScreen"].Title}</span>
        </div>

        <div className="Loginscreen_mid" >
          <input className="Loginscreen_inputBox" type="Text" placeholder={data["LoginModule"]["SignUpScreen"].components.Textbox_UserName} />
          <input className="Loginscreen_inputBox" type="Text" placeholder={data["LoginModule"]["SignUpScreen"].components.Textbox_Email} />
          <input className="Loginscreen_inputBox" type="Text" placeholder={data["LoginModule"]["SignUpScreen"].components.Textbox_Password} />
          <input className="Loginscreen_inputBox" type="Text" placeholder={data["LoginModule"]["SignUpScreen"].components.Textbox_Password2} />
          <button className="Loginscreen_button" >
            {data["LoginModule"]["SignUpScreen"].components.Button}</button>
        </div>

        <div className="Loginscreen_bottom">
          <footer className="Loginscreen_Footer">
            <span style={{ fontSize: 15, color: "grey" }}>{data["LoginModule"]["SignUpScreen"].components.Bottom_Text} </span>
            <span>{data["LoginModule"]["SignUpScreen"].components.Bottom_Link} </span>
          </footer>
        </div>

      </div>
    )
  }
}

export class Layout2 extends React.Component {

  render() {
    return (
      <div className="productPrediv_Loginscreen">
        <div className="Layout2_Container">

          <div className="upper">
            <div className="logoDiv">
              <span className="logospan logoText">AppMaker</span>
            </div>
            <div className="welcomeDiv">
              <span className="welcome welcomeText">{data["LoginModule"]["SignUpScreen"].Title}</span>
            </div>
            <div className="inputDiv">
              <div style={{ alignSelf: "center" }}>

                <div className="item" >
                  <input className="inputBox" placeholderTextColor='#ffffff' placeholder={data["LoginModule"]["SignUpScreen"].components.Textbox_UserName} />
                </div>

                <div className="item" >
                  <input className="inputBox" placeholderTextColor='#ffffff' placeholder={data["LoginModule"]["SignUpScreen"].components.Textbox_Email} />
                </div>

                <div className="item" >
                  <input securespanEntry={true} className="inputBox" placeholderTextColor='#ffffff' placeholder={data["LoginModule"]["SignUpScreen"].components.Textbox_Password} />
                </div>

                <div className="item" >
                  <input securespanEntry={true} placeholderTextColor='#ffffff' className="inputBox" placeholder={data["LoginModule"]["SignUpScreen"].components.Textbox_Password2} />
                </div>

                <div className="item" >
                  <button className="button" onPress={() => this.signIn()}>
                    <span className="buttonText">{data["LoginModule"]["SignUpScreen"].components.Button}</span>
                  </button>
                </div>
              </div>

            </div>
          </div>
          <div className="lower" >
            <span className="bottomText">{data["LoginModule"]["SignUpScreen"].components.Bottom_Text}</span>
            <span className="bottomText bottomText2" onPress={() => this.props.navigation.navigate('signup')}>
              {data["LoginModule"]["SignUpScreen"].components.Bottom_Link}</span>
          </div >
        </div>
      </div>
    )
  }
}