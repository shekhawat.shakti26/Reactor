import React, { Component } from 'react';
// import logo from './logo.svg';
import {
  BrowserRouter as Router,
  Route, Switch,
} from 'react-router-dom';
import './App.css';
import UI from './routes/UI/UI';
import CreateNewApp from './routes/UI/CreateNewApp';
import AboutUs from './routes/AboutUs/About';
import AppMaker from './routes/AppMaker/AppMaker';
import Contact from './routes/Contact/Contact';
import NotFound from './routes/NotFound/NotFound';
import Login from './routes/Login/Login';
import SignUp from './routes/SignUp/SignUp.js';
class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Switch>
            <Route path='/about' component={AboutUs} />
            <Route exact path="/ui" component={UI} />
            <Route exact path="/CreateNewApp" component={CreateNewApp} />
            <Route path='/News_App/' component={AppMaker} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/SignUp" component={SignUp} />
            <Route exact path="/contact" component={Contact} />
            <Route component={NotFound} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
