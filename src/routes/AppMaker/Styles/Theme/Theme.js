import React from 'react';
import './Theme.css';
import '../../GlobalStyleSheet.css';
import { Scrollbars } from 'react-custom-scrollbars';
import { SketchPicker } from 'react-color';
import * as appData from '../../variableData';
var userApp_JSON = localStorage.getItem('JSON');
var AppJson = JSON.parse(userApp_JSON);

const Theme = () => (

    <div className='MainContainer'>
        <div className="UpperContainer">
            <div className="Heading"> <h2>Customize theme</h2></div>
        </div>
        <Scrollbars style={{ height: '420px' }} >
            <div className="lowerContainer">
                <Content />
            </div></Scrollbars>
    </div>

);


class Content extends React.Component {
    constructor(props) {
        super(props)
        var userApp_JSON = localStorage.getItem('JSON');
        var AppJson = JSON.parse(userApp_JSON);
        this.state = {
            StatusBar:  AppJson["Theme"]["StatusBar"],
            BackgroundColor: AppJson["Theme"]["BackgroundColor"],
            HeaderColor: AppJson["Theme"]["HeaderColor"],
            HeadingColor: AppJson["Theme"]["HeadingColor"],
            HeadingFont: AppJson["Theme"]["HeadingFont"],
            TitleColor: AppJson["Theme"]["TitleColor"],
            TitleFont: AppJson["Theme"]["TitleFont"],
            ButtonColor:AppJson["Theme"]["ButtonColor"],
            ButtonTextColor:AppJson["Theme"]["ButtonTextColor"],
            ButtonTextFont:AppJson["Theme"]["ButtonTextFont"],
            NormalTextColor: AppJson["Theme"]["NormalTextColor"],
            NormalTextFont: AppJson["Theme"]["NormalTextFont"],
            LinkTextFont:AppJson["Theme"]["LinkTextFont"],
            LinkTextColor: AppJson["Theme"]["LinkTextColor"],
            TextInputColor: AppJson["Theme"]["TextInputColor"],
            TextInputFont: AppJson["Theme"]["TextInputFont"],
            TextInputBackgroundColor: AppJson["Theme"]["TextInputBackgroundColor"],
            SubHeadingColor:AppJson["Theme"]["SubHeadingColor"],
            SubHeadingFont: AppJson["Theme"]["SubHeadingFont"],
            Component_Id: "",
            SketchPickerVisible: false,
            obj: {},
            FontFamilyList: ['normal', 'notoserif', 'sans-serif', 'sans-serif-light', 'sans-serif-thin',
                'sans-serif-condensed', 'sans-serif-medium', 'serif', 'Roboto', 'monospace',],
        };
        console.log(this.state.ButtonTextFont);
    }
    componentDidMount() {
        console.log("componentdidmount AppJson");
        // console.log(AppJson);
    }
    handleChangeComplete = (color) => {
        var userApp_JSON = localStorage.getItem('JSON');
        var AppJson = JSON.parse(userApp_JSON);
        var obj = {};
        var component = this.state.obj.componentName;
        obj[component] = color.hex;
        console.log(component);
        AppJson["Theme"][component] = color.hex;      
        this.setState(obj);
        localStorage.setItem('JSON', JSON.stringify(AppJson));
        console.log("AppJson in themes");
        console.log(AppJson);
    };
    CallDisplay_Hide(component_id, componentName) {
        if (this.state.SketchPickerVisible === false) {
            this.setState({
                Component_Id: component_id,
                SketchPickerVisible: true
            });
            this.Display_Hide(component_id, componentName);
        }
        else {
            this.setState({ SketchPickerVisible: false });
            this.Display_Hide(this.state.Component_Id, componentName);
        }

    }
    Reset() {
        var userApp_JSON = localStorage.getItem('JSON');
        var AppJson = JSON.parse(userApp_JSON);
        AppJson["Theme"] = {
            "StatusBar": "#a50000",
            "BackgroundColor": "#a50000",
            "HeaderColor": "#a50000",
            "HeadingColor": "white",
            "HeadingFont": "serif",
            "TitleColor": "white",
            "TitleFont": "serif",
            "ButtonColor": "white",
            "ButtonTextColor": "#a50000",
            "ButtonTextFont": "serif",
            "NormalTextColor": "black",
            "NormalTextFont": "serif",
            "LinkTextFont": "serif",
            "LinkTextColor": "white",
            "TextInputColor": "white",
            "TextInputFont": "serif",            
            "TextInputBackgroundColor": "rgb(205,97,95)",
            "SubHeadingColor": "black",
            "SubHeadingFont": "serif"
        }
        localStorage.setItem('JSON', JSON.stringify(AppJson));
    }
    handleChange= (e,name) => {
        console.log(e.target.value);
        console.log(name);
        var userApp_JSON = localStorage.getItem('JSON');
        var AppJson = JSON.parse(userApp_JSON);
        AppJson["Theme"][name] = e.target.value;
        console.log(AppJson);
        localStorage.setItem('JSON', JSON.stringify(AppJson));
           
      }   
    Display_Hide(component_id, componentName) {
        var SketchPicker = document.getElementById(component_id);

        if (SketchPicker.style.display === "none") {
            SketchPicker.style.display = "block";
            var obj = {
                componentName: componentName
            }
            this.setState({
                obj: obj
            })
        } else {
            SketchPicker.style.display = "none";
        }
    }
    render() {
        return (
            <div >
                <div className="heading" >
                    <button className="Reset_button" onClick={() => this.Reset()}>Default</button>
                </div>

                <div className="component">

                    <div className="Component_left">
                        <label className="LabelText">  Background :  </label>
                    </div>

                    <div className="Component_middle" onClick={() => this.CallDisplay_Hide("component_1", "BackgroundColor")}>
                        <div style={{ backgroundColor: this.state.BackgroundColor, height: 20, width: 20, marginRight: 15 }}>
                        </div>
                        <label>{this.state.BackgroundColor}</label>
                    </div>

                    <div className="Component_right">
                        <div style={{ display: "none" }} id="component_1">
                            <SketchPicker
                                color={this.state.BackgroundColor}
                                onChangeComplete={this.handleChangeComplete} />
                        </div> </div> </div>

                <div className="component">

                    <div className="Component_left" >
                        <label className="LabelText"> StatusBar :  </label>
                    </div>

                    <div className="Component_middle" onClick={() => this.CallDisplay_Hide("component_2", "StatusBar")}>
                        <div style={{ backgroundColor: this.state.StatusBar, height: 20, width: 20, marginRight: 15 }}>
                        </div>
                        <label>{this.state.StatusBar}</label>
                    </div>

                    <div className="Component_right">
                        <div style={{ display: "none" }} id="component_2">
                            <SketchPicker
                                color={this.state.StatusBar}
                                onChangeComplete={this.handleChangeComplete} />
                        </div></div></div>

                <div className="component">

                    <div className="Component_left" >
                        <label className="LabelText"> Header :  </label>
                    </div>

                    <div className="Component_middle" onClick={() => this.CallDisplay_Hide("component_3", "HeaderColor")}>
                        <div style={{ backgroundColor: this.state.HeaderColor, height: 20, width: 20, marginRight: 15 }}>
                        </div>
                        <label>{this.state.HeaderColor}</label>
                    </div>

                    <div className="Component_right">
                        <div style={{ display: "none" }} id="component_3">
                            <SketchPicker
                                color={this.state.HeaderColor}
                                onChangeComplete={this.handleChangeComplete} />
                        </div></div></div>

                <div className="component">

                    <div className="Component_left">
                        <label className="LabelText"> Button:  </label>
                    </div>

                    <div className="Component_middle" onClick={() => this.CallDisplay_Hide("component_4", "ButtonColor")}>
                        <div style={{ backgroundColor: this.state.ButtonColor, height: 20, width: 20, marginRight: 15 }}>
                        </div>
                        <label>{this.state.ButtonColor}</label>
                    </div>

                    <div className="Component_right">
                        <div style={{ display: "none" }} id="component_4">
                            <SketchPicker
                                color={this.state.ButtonColor}
                                onChangeComplete={this.handleChangeComplete} />
                        </div> </div> </div>

                <div className="component">

                    <div className="Component_left" >
                        <label className="LabelText">InputBox:  </label>
                    </div>

                    <div className="Component_middle" onClick={() => this.CallDisplay_Hide("component_6", "TextInputBackgroundColor")}>
                        <div style={{ backgroundColor: this.state.TextInputBackgroundColor, height: 20, width: 20, marginRight: 15 }}>
                        </div>
                        <label>{this.state.TextInputBackgroundColor}</label>
                    </div>

                    <div className="Component_right">
                        <div style={{ display: "none" }} id="component_6">
                            <SketchPicker
                                color={this.state.TextInputBackgroundColor}
                                onChangeComplete={this.handleChangeComplete} />
                        </div></div></div>

                        <div className="component">

                    <div className="Component_left" >
                        <label className="LabelText"> Input Text:  </label>
                    </div>

                    <div className="Component_middle" onClick={() => this.CallDisplay_Hide("component_5", "TextInputColor")}>
                        <div style={{ backgroundColor: this.state.TextInputColor, height: 20, width: 20, marginRight: 15 }}>
                        </div>
                        <label>{this.state.TextInputColor}</label>
                    </div>

                    <div className="Component_right">
                    <div className="Font_selector">
                            <select  name={this.state.TextInputFont} value={this.state.TextInputFont}  onChange={(e)=>this.handleChange(e,"TextInputFont")} >
                                <option value="" disabled="disabled" >Choose fonts..</option>
                                {this.state.FontFamilyList.map((font, index)=> { return (<option key={index}  value={font}>{font}</option>) })}
                            </select>
                        </div>
                        <div style={{ display: "none" }} id="component_5">
                            <SketchPicker
                                color={this.state.TextInputColor}
                                onChangeComplete={this.handleChangeComplete} />
                        </div></div></div>


                <div className="component">

                    <div className="Component_left" >
                        <label className="LabelText"> Title:  </label>
                    </div>

                    <div className="Component_middle" onClick={() => this.CallDisplay_Hide("component_7", "TitleColor")}>
                        <div style={{ backgroundColor: this.state.TitleColor, height: 20, width: 20, marginRight: 15 }}>
                        </div>
                        <label>{this.state.TitleColor}</label>
                    </div>

                    <div className="Component_right">
                    <div className="Font_selector">
                             <select name={this.state.TitleFont} value={this.state.TitleFont} onChange={(e)=>this.handleChange(e,"TitleFont")} >
                                <option value="" disabled="disabled" >Choose fonts..</option>
                                {this.state.FontFamilyList.map((font, index)=> { return (<option key={index} value={font}>{font}</option>) })}
                            </select>
                        </div>
                        <div style={{ display: "none" }} id="component_7">
                            <SketchPicker
                                color={this.state.TitleColor}
                                onChangeComplete={this.handleChangeComplete} />
                        </div></div></div>

                <div className="component">

                    <div className="Component_left">
                        <label className="LabelText">Heading:  </label>
                    </div>

                    <div className="Component_middle" onClick={() => this.CallDisplay_Hide("component_8", "HeadingColor")}>
                        <div style={{ backgroundColor: this.state.HeadingColor, height: 20, width: 20, marginRight: 15 }}>
                        </div>
                        <label>{this.state.HeadingColor}</label>
                    </div>

                    <div className="Component_right">
                    <div className="Font_selector">
                            <select name={this.state.HeadingFont} value={this.state.HeadingFont} onChange={(e)=>this.handleChange(e,"HeadingFont")} >
                                <option value="" disabled="disabled" >Choose fonts..</option>
                                {this.state.FontFamilyList.map((font, index)=> { return (<option key={index}  value={font}>{font}</option>) })}
                            </select>
                        </div>
                        <div style={{ display: "none" }} id="component_8">
                            <SketchPicker
                                color={this.state.HeadingColor}
                                onChangeComplete={this.handleChangeComplete} />
                        </div> </div> </div>


                <div className="component">

                    <div className="Component_left" >
                        <label className="LabelText"> Sub Heading:  </label>
                    </div>

                    <div className="Component_middle" onClick={() => this.CallDisplay_Hide("component_9", "SubHeadingColor")}>
                        <div style={{ backgroundColor: this.state.SubHeadingColor, height: 20, width: 20, marginRight: 15 }}>
                        </div>
                        <label>{this.state.SubHeadingColor}</label>
                    </div>

                    <div className="Component_right">
                    <div className="Font_selector">
                    <select name={this.state.SubHeadingFont} value={this.state.SubHeadingFont} onChange={(e)=>this.handleChange(e,"SubHeadingFont")} >
                                <option value="" disabled="disabled" >Choose fonts..</option>
                                {this.state.FontFamilyList.map((font, index)=> { return (<option key={index}  value={font}>{font}</option>) })}
                            </select>
                        </div>
                        <div style={{ display: "none" }} id="component_9">
                            <SketchPicker
                                color={this.state.SubHeadingColor}
                                onChangeComplete={this.handleChangeComplete} />
                        </div></div></div>

                <div className="component">

                    <div className="Component_left" >
                        <label className="LabelText"> Button Text:  </label>
                    </div>

                    <div className="Component_middle" onClick={() => this.CallDisplay_Hide("component_10", "ButtonTextColor")}>
                        <div style={{ backgroundColor: this.state.ButtonTextColor, height: 20, width: 20, marginRight: 15 }}>
                        </div>
                        <label>{this.state.ButtonTextColor}</label>
                    </div>

                    <div className="Component_right">
                    <div className="Font_selector">
                    <select name={this.state.ButtonTextFont} value={this.state.ButtonTextFont} onChange={(e)=>this.handleChange(e,"ButtonTextFont")} >
                                <option value="" disabled="disabled" >Choose fonts..</option>
                                {this.state.FontFamilyList.map((font, index)=> { return (<option key={index}  value={font} >{font}</option>) })}
                            </select>
                        </div>
                        <div style={{ display: "none" }} id="component_10">
                            <SketchPicker
                                color={this.state.ButtonTextColor}
                                onChangeComplete={this.handleChangeComplete} />
                        </div></div></div>

                <div className="component">

                    <div className="Component_left">
                        <label className="LabelText"> Normal Text:  </label>
                    </div>

                    <div className="Component_middle" onClick={() => this.CallDisplay_Hide("component_11", "NormalTextColor")}>
                        <div style={{ backgroundColor: this.state.NormalTextColor, height: 20, width: 20, marginRight: 15 }}>
                        </div>
                        <label>{this.state.NormalTextColor}</label>
                    </div>

                    <div className="Component_right">
                    <div className="Font_selector">
                    <select name={this.state.NormalTextFont} value={this.state.NormalTextFont}  onChange={(e)=>this.handleChange(e,"NormalTextFont")} >
                                <option value="" disabled="disabled" >Choose fonts..</option>
                                {this.state.FontFamilyList.map((font, index)=> { return (<option key={index}  value={font}>{font}</option>) })}
                            </select>
                        </div>
                        <div style={{ display: "none" }} id="component_11">
                            <SketchPicker
                                color={this.state.NormalTextColor}
                                onChangeComplete={this.handleChangeComplete} />
                        </div> </div> </div>

                <div className="component">

                    <div className="Component_left" >
                        <label className="LabelText"> Link Text:  </label>
                    </div>

                    <div className="Component_middle" onClick={() => this.CallDisplay_Hide("component_12", "LinkTextColor")}>
                        <div style={{ backgroundColor: this.state.LinkTextColor, height: 20, width: 20, marginRight: 15 }}>
                        </div>
                        <label>{this.state.LinkTextColor}</label>
                    </div>

                    <div className="Component_right">
                        <div className="Font_selector">
                        <select name={this.state.LinkTextFont} value={this.state.LinkTextFont} onChange={(e)=>this.handleChange(e,"LinkTextFont")} >
                                <option value="" disabled="disabled" >Choose fonts..</option>
                                {this.state.FontFamilyList.map((font, index)=> { return (<option key={index} value={font}>{font}</option>) })}
                            </select>
                        </div>
                        <div style={{ display: "none" }} id="component_12">
                            <SketchPicker
                                color={this.state.LinkTextColor}
                                onChangeComplete={this.handleChangeComplete} />
                        </div></div></div>

            </div>
        );
    }
}
export default Theme;