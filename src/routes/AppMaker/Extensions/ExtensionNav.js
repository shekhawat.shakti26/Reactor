import React from 'react'
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';
import '../GlobalStyleSheet.css';
// import AboutApp from './About/about';
// import Events from './Events/Events';
import CreateKey from './CreateKey/CreateKey';
import DataBase from './DataBase/DataBase';
// import { Scrollbars } from 'react-custom-scrollbars';
var userApp_JSON = localStorage.getItem('JSON');
var AppJson = JSON.parse(userApp_JSON);
const ExtensionRoutes = [
  {
    path: '/News_App/extension/DataBase',
    exact: true,
    sidebar: () => <div>Products!</div>,
    main: () => <DataBase />
  },
  {
    path: '/News_App/extension/CreateKey',
    sidebar: () => <div>Events!</div>,
    main: () => <CreateKey />
  },
  //   { path: '/extension/photos',
  //     sidebar: () => <div>Photos!</div>,
  //     main: () => <Photos/>
  //   },
  //   { path: '/extension/about',
  //   sidebar: () => <div>About!</div>,
  //   main: () => <AboutApp/>
  // }
]


const ExtensionNavBar = () => (
  <Router>
    <div className='Nav_MainContainer'>
      <div className='Nav_left '>

        <div className='Nav_heading'>
          <div style={{ flex: 6 }}>
            <h4>Extension</h4>
          </div>

        </div>
        <div className='Nav_List'>
          <div className='Nav_Item'><i class="fa fa-square-o" aria-hidden="true"></i>
            <Link className='Nav_linkscreen' to="/News_App/extension/DataBase"> DataBase</Link></div>
        </div>

        <div className='Nav_List'>
          <div className='Nav_Item'><i class="fa fa-square-o" aria-hidden="true"></i>
            <Link className='Nav_linkscreen' to="/News_App/extension/CreateKey"> CreateKey</Link></div>
        </div>
        {/*
<div className='Extension_List'>
<div className='Extension_Item'><i class="fa fa-square-o" aria-hidden="true"></i>
<Link className='Extension_linkscreen' to="/extension/photos"> Photos</Link></div>
</div>

<div className='Extension_List'>
<div className='Extension_Item'><i class="fa fa-square-o" aria-hidden="true"></i>
<Link className='Extension_linkscreen' to="/extension/about"> About</Link></div>
</div>    */}
      </div>
      <div className='Nav_right '>
        {ExtensionRoutes.map((route, index) => (

          <Route
            key={index}
            path={route.path}
            exact={route.exact}
            component={route.main}
          />
        ))}
      </div>
    </div>
  </Router>
)

export default ExtensionNavBar