import React from 'react'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import '../GlobalStyleSheet.css';
import Photos from './Photos/Photos';
import Theme from './Theme/Theme';
// import { Scrollbars } from 'react-custom-scrollbars';
var userApp_JSON = localStorage.getItem('JSON');
var AppJson = JSON.parse(userApp_JSON);
const StyleRoutes = [
  {
    path: '/News_App/styles/',
    exact: true,
    sidebar: () => <div>Products!</div>,
    main: () => <Theme />
  },
  {
    path: '/News_App/styles/Photos/',
    sidebar: () => <div>Photos!</div>,
    main: () => <Photos />
  },
]


const StyleNavBar = () => (
  <Router>
    <div className='Nav_MainContainer'>
      <div className='Nav_left '>

        <div className='Nav_heading'>
          <div style={{ flex: 6 }}>
            <h4>Styles</h4>
          </div>
        </div>
        <div className='Nav_List'>
          <div className='Nav_Item'><i className="fa fa-square-o" aria-hidden="true"></i>
            <Link className='Nav_linkscreen' to="/News_App/styles/"> Customize theme</Link></div>
        </div>

        <div className='Nav_List'>
          <div className='Nav_Item'><i className="fa fa-square-o" aria-hidden="true"></i>
            <Link className='Nav_linkscreen' to="/News_App/styles/Photos/"> Splash / Icon</Link></div>
        </div>
      </div>
      <div className='Nav_right '>
        {StyleRoutes.map((route, index) => (

          <Route
            key={index}
            path={route.path}
            exact={route.exact}
            component={route.main}
          />
        ))}
      </div>
    </div>
  </Router>
);

export default StyleNavBar