import React from 'react';
import './UI.css'
import Footer from '../../components/Footer/Footer'
import Header from '../../components/Header';
import { Link } from 'react-router-dom';
import screen_default from '../../data/News App/screen_default.jpg';

class UI extends React.Component {

  componentDidMount() {
    var login = false;
    if (login) {
      this.props.history.push('/app-maker/')
    }

  }
  render() {
    return (
      <div className="UI_Root">
        <Header />
        <div className="UI_Container">
          <div className="UI_Instructo">
            <h1 className="UI_Textstyle">
              Choose a template based on the features, or just the looks.
              Customize everything later!!
            </h1>
            <h3 className="UI_Textstyle">
              Creating a conference, meet-up or concert app?<br /> Provide your
              attendees with the information they need,<br /> and ways to share and
              augment their experience.<br />
              <i style={{ fontSize: 16 }}>(Currently we have only one :p)</i>
            </h3>
            <Link className="UI-link" to="/CreateNewApp/">
              <button
                style={{
                  backgroundColor: '#444f6c', width: '25%', height: 50,
                  borderRadius: 10, letterSpacing: 1, color: '#fff', marginTop: 24,
                }}
                className="btn btn-primary">   Continue</button>
            </Link>
          </div>
          <div className="UI_Right">
            <div className="UI_DeviceMock">
              <div className="Post-thumbnail">
                <img
                  src={screen_default}
                  srcSet="https://new.shoutem.com/wp-content/uploads/2017/01/events-tuparro-tabbar-2-272x484.jpg 272w, https://new.shoutem.com/wp-content/uploads/2017/01/events-tuparro-tabbar-2-544x968.jpg 544w"
                  sizes="240px"
                  alt=""
                  style={{ borderTopWidth: 4, borderColor: '#007EA7' }}
                />
              </div>{' '}
            </div>
          </div>
          {/* Body Ends */}
        </div>
        <Footer />
      </div>
    );
  }
}

export default UI;