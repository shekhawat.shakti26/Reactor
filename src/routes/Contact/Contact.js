import React from 'react';
import './Contact.css';
import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import { RingLoader, BarLoader, ScaleLoader, SyncLoader} from 'react-spinners';
import { setTimeout } from 'timers';

class Contact extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      AppList: [],
      loading: true
    }

  }
on() {
   var box= document.getElementById('overlay');
   box.style.display = "block";
}
 off() {
  var box= document.getElementById('overlay');
  box.style.display = "none";
}
  render() {
    return (
      <div className="Contact-root">
        <Header />
        <div className="Contact-container">
          <h1>Right now, You can't contact us!!!!</h1>
          <p>But, soon you'll be able to</p>
          {/* <button onClick={()=>this.on()}/> */}
        
          <div id="overlay" className="overlay">
            <div id="loader" className="loader">
            <RingLoader style={{ marginLeft: 15,height:500,width:200 }} color={'white'} loading={this.state.loading} />
            <BarLoader style={{ marginLeft: 15 }} color={'#fff'} loading={this.state.loading} />
            <ScaleLoader style={{ marginLeft: 15 }} color={'#fff'} loading={this.state.loading} />
            <SyncLoader style={{ marginLeft: 15 }} color={'#123abc'} loading={this.state.loading} />
              </div>
                </div>
        </div>
        <div className="Contact-footer">
          <Footer />
        </div>
      </div>
    );
  }
}

export default Contact;