import React from 'react';
import { Scrollbars } from 'react-custom-scrollbars';
import './DescriptionLayout.css';
var userApp_JSON = localStorage.getItem('JSON');
var AppJson = JSON.parse(userApp_JSON);
var data = AppJson;
var content = " A Delhi court on December 21 acquitted all accused, including former Telecom Minister A. Raja and DMK Rajya Sabha member Kanimozhi of corruption and money laundering charges in the 2G spectrum allocation cases.Special Judge O.P. Saini said:  'I have no hesitation in holding that the the prosecution has failed to prove the charges beyond reasonable doubt. So all are acquitted.'The court also acquitted all the accused of corruption charges in the Essar Group Loop Telecom case.The CBI had charged Mr. Raja, Ms. Kanimozhi, then Telecom Secretary Siddhartha Behura, Mr. Raja's former private secretary R.K. Chandolia, Unitech Wireless Managing Director Sanjay Chandra, Swan Telecom director and DB Realty MD Vinod Goenka, and Reliance Anil Dhirubhai Ambani Group’s (R-ADAG) Gautam Doshi, Hari Nair and Surendra Pipara, Swan telecom promoters Shahid Usman Balwa and Vinod Goenka, directors of Kusegaon Fruits and Vegetables Pvt. Ltd. Asif Balwa and Rajiv Agarwal, Bollywood producer Karim Morani, Kalaignar TV director Sharad Kumar.The accused also include three companies namely, Swan Telecom (now Etisalat DB), Unitech Wireless (Tamil Nadu) Pvt Ltd and Reliance Telecom Ltd.According to the CBI charge sheet, the wrongful acts of the accused deprived the exchequer of revenues amounting to ₹30,984.55 crore.Both the CBI and the Enforcement Directorate have said they would appeal against thr trial court verdict. CBI spokesperson Abhishek Dayal said they would appeal against the special court verdict in Delhi High Court. Similarly, ED Director Karnal Singh said the agency would go on appeal against the verdict. He said all the evidence adduced by the ED were not appreciated by the court.On April 25, 2014, the ED filed a charge sheet naming Mr. Raja, Ms. Kanimozhi, Dayalu Ammal, Swan Telecom promoters Shahid Usman Balwa and Vinod Goenka, directors of Kusegaon Fruits and Vegetables Pvt. Ltd. Asif Balwa and Rajiv Agarwal, Bollywood producer Karim Morani, Kalaignar TV director Sharad Kumar and P. Amirthan as accused in the case.The companies named are Swan Telecom Pvt. Ltd. (STPL), Kusegaon Realty Pvt. Ltd., Cineyug Media and Entertainment Pvt. Ltd. (Cineyug Films), Kalaignar TV (KTV) Pvt. Ltd., Dynamix Realty, Eversmile Construction Company Private Limited, Conwood Construction and Developers (P) Ltd., DB Realty Ltd., and Nihar Construction Pvt. Ltd.The ED filed the case relating to money laundering, alleging that a conspiracy was hatched by Mr. Raja, Ms. Kanimozhi, Ms. Dayalu and others and that ₹200 crore were the proceeds of the crime.The CBI has alleged that the promoters of the Essar Group were the real investors and beneficiaries of the spectrum and licences were issued to Loop Telecom, a front company of Essar to acquire 2G licences and spectrum in 2008.Loop Telecom promoter IP Khaitan, his wife Kiran Khaitan, Essar Group promoters Ravi and Anshuman Ruia and group official Vikash Saraf are facing trial in the case for hatching a conspiracy to cheat the government for obtaining spectrum licence. The accused have denied the charges."

export class Layout1 extends React.Component {
    render() {
        return (
            <div className="productPreview_NewsDescription">
                <div className="NewsDescription_header">
                    <div className="header_back">
                        <i class="fa fa-arrow-left" aria-hidden="true" style={{ marginLeft: 17 }}></i></div>
                    <div className="header_title">News</div>
                    {/* <div className="header_next">
        <i class="fa fa-arrow-right" aria-hidden="true"></i></div> */}
                </div>
                <Scrollbars style={{ height: '90%' }}>
                    <div className="NewsDescription_content">
                        <div className="NewsDescription_headlines">
                            <span style={{ flex: 7, alignSelf: "center", fontWeight: 600 }}>"A.Raja,acquitted in 2G spectrum allocation cases"</span>
                            <span style={{ flex: 3, color: "gray", fontSize: 10, }}>21 December 2017</span>
                        </div>
                        <div className="NewsDescription_imageContainer">
                            <img src={"http://www.thehindu.com/incoming/article22122043.ece/alternates/FREE_660/Untitledpng"} alt="..." className="NewsDescription_image" />
                        </div>
                        <div className="NewsDescription_description">
                            <span>{content}</span>
                        </div>
                    </div>
                </Scrollbars>
            </div>
        )
    }
}


export class Layout2 extends React.Component {

    render() {
        return (
            <div className="productPreview_NewsDescription">
                <div className="NewsDescription_header">
                    <div className="header_back">
                        <i class="fa fa-arrow-left" aria-hidden="true" style={{ marginLeft: 17 }}></i></div>
                    <div className="header_title">News</div>
                    {/* <div className="header_next">
        <i class="fa fa-arrow-right" aria-hidden="true"></i></div> */}
                </div>
                <Scrollbars style={{ height: '90%' }}>
                    <div className="NewsDescription_content">
                        <div className="NewsDescription_headlines">
                            <span style={{ flex: 7, alignSelf: "center", fontWeight: 600 }}>"A.Raja,acquitted in 2G spectrum allocation cases"</span>
                        </div>
                        <div className="NewsDescription_imageContainer">
                            <img src={"http://www.thehindu.com/incoming/article22122043.ece/alternates/FREE_660/Untitledpng"} alt="..." className="NewsDescription_image" />
                        </div>
                        <div className="NewsDescription_description">
                            <span>{content}</span>
                        </div>
                    </div>
                </Scrollbars>
            </div>
        )
    }
}