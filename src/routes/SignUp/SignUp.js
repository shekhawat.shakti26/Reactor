import React from 'react';
import './SignUp.css';
import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import firebaseApp from "../../FireBase";
import { ScaleLoader} from 'react-spinners';

class SignUp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
    }
  }
  handleChange(e) {    
    var varname=e.target.name;
    var value = e.target.value;  
    this.setState({[varname]:value});    
}

  async signUp(e) {
    e.preventDefault()
    var name = this.state.Name;
    var email = this.state.Email;
    var password = this.state.password;
    var box = document.getElementById('overlay');

    if (!name || !email || !password ) {
      alert('Enter All Details');
    }
    else {
      try {
        box.style.display = "block";
        console.log(email + " " + password);
        // firebaseApp.auth().createUserWithEmailAndPassword(email, password)  //firebase signup authenciation
          // .then((firebaseUser) => {
            // console.log(firebaseUser);
            // localStorage.setItem('UserId', firebaseUser.uid);
            localStorage.setItem('UserId', "Shakti123");
            // this.props.history.push('/ui')
            //   console.log("firebaseUser");
            // console.log(firebaseUser.uid);
            // firebaseApp.database().ref("/user/" + firebaseUser.uid + "/UserInfo/").set({ //store messages in database
              // Name: name,
              // Email: email,
              // UserId: firebaseUser.uid
            // }).then(() => {
              box.style.display = "none";
              window.location = '/ui';
            // });

          // }, (error) => { // errror handler if error accur during login
          //   var errorCode = error.code;
          //   var errorMessage = error.message;
          //   box.style.display = "none";
          //   if (errorCode === 'auth/wrong-password') {
          //     alert('Wrong password!');
          //   }
          //   else if (errorCode === 'auth/invalid-email') {
          //     alert('Invalid Email!');
          //   }
          //   else if (errorCode === 'auth/user-not-found') {
          //     alert('User Not Found!');
          //   }
          //   else {
          //     alert(errorMessage);
          //   }
          // })
      }
      catch (error) {
        box.style.display = "none";
        console.log(error.toString())
      }
    }
  }
  render() {
    return (
      <div className="SignUp-root">
        <Header />
        <div className="SignUp-container">

          <div id="overlay" className="overlay">
            <div id="loader" className="loader">
              {/* <RingLoader color={'#FFF'} loading="true" /> */}
              {/* <BarLoader color={'#123abc'} loading="true" /> */}
              <ScaleLoader  color={'#123abc'} loading="true" />
              {/* <SyncLoader  color={'#123abc'} loading="true"/> */}
            </div>
          </div>

          <h1>SignUp</h1>
          <p className="SignUp-lead">
            Register with your email address.
          </p>

          <form onSubmit={(event)=>this.signUp(event)}>
            <div className="SignUp-formGroup">
              <label className="SignUp-label" htmlFor="Name">
                Name:
                <input className="SignUp-input" id="Name" type="text"
                onChange={(event)=>this.handleChange(event)}
                  name="Name" autoFocus // eslint-disable-line jsx-a11y/no-autofocus          
                />
              </label>
            </div>

            <div className="SignUp-formGroup">
              <label className="SignUp-label" htmlFor="usernameOrEmail">
                Email address:
                <input className="SignUp-input" id="usernameOrEmail" type="text"
                onChange={(event)=>this.handleChange(event)}
                  name="Email" />
              </label>
            </div>
            <div className="SignUp-formGroup">
              <label className="SignUp-label" htmlFor="password">
                Password:
                <input className="SignUp-input" id="password" type="password"
                onChange={(event)=>this.handleChange(event)}  name="password" />
              </label>
            </div>
            <div className="SignUp-formGroup">
              <button className="btn SignUp-button" type="submit">
                Sign Up
              </button>
            </div>
          </form>
        </div>
        <div style={{ bottom: 0, left: 0, position: 'absolute', width: '100%' }}>
          <Footer />
        </div>
      </div>
    );
  }
}

export default SignUp;