import React from 'react';
import './LoginLayout.css';
// import * as appData from '../../../variableData';
// import AppJson from '../Modules';
var userApp_JSON = localStorage.getItem('JSON');
var AppJson = JSON.parse(userApp_JSON);
var data = AppJson;

export class Layout1 extends React.Component {
    constructor(props) {
        super(props)
        var userApp_JSON = localStorage.getItem('JSON');
        var AppJson = JSON.parse(userApp_JSON);
        this.state = {
            json: AppJson,
            background: data["Theme"].BackgroundColor,
        }
        console.log("layoutjson")
        console.log(this.state.json)
    }


    render() {
        return (
            <div className="productPrediv_Loginscreen" style={{ backgroundColor: this.state.background }}
            >

                <div className="Loginscreen_top">
                    <img className="Loginscreen_logo" src={require("../../../../../data/News App/logo.jpg")} alt="" />
                    <span className="Loginscreen_Main_Heading">{data["LoginModule"]["LoginScreen"].Title}</span>
                </div>

                <div className="Loginscreen_mid" >
                    <input className="Loginscreen_inputBox" type="span" placeholder={data["LoginModule"]["LoginScreen"].components.Textbox_Email} />
                    <input className="Loginscreen_inputBox" type="span" placeholder={data["LoginModule"]["LoginScreen"].components.Textbox_Password} />
                    <button className="Loginscreen_button">{data["LoginModule"]["LoginScreen"].components.Button}</button>
                </div>

                <div className="Loginscreen_bottom">
                    <footer className="Loginscreen_Footer">
                        <span style={{ fontSize: 15, color: "grey" }}>{data["LoginModule"]["LoginScreen"].components.Bottom_Text}</span>
                        <span> {data["LoginModule"]["LoginScreen"].components.Bottom_Link} </span>
                    </footer>
                </div>

            </div>
        );
    }
}


export class Layout2 extends React.Component {

    render() {
        return (
            <div className="productPrediv_Loginscreen">
                <div className="Layout2_Container">

                    <div className="upper">
                        <div className="logoDiv">
                            <span className="logospan logoText">AppMaker</span>
                        </div>
                        <div className="welcomeDiv">
                            <span className="welcome welcomeText">{data["LoginModule"]["LoginScreen"].Title}</span>
                        </div>
                        <div className="inputDiv">
                            <div style={{ alignSelf: "center" }}>

                                <div className="item" >
                                    <input className="inputBox" placeholderspanColor='#ffffff' placeholder={data["LoginModule"]["LoginScreen"].components.Textbox_Email} />
                                </div>

                                <div className="item" >
                                    <input securespanEntry={true} placeholderspanColor='#ffffff' className="inputBox" underlineColorAndroid='rgba(0,0,0,0)' placeholder={data["LoginModule"]["LoginScreen"].components.Textbox_Password} />
                                </div>

                                <div className="item" >
                                    <button className="button" onPress={() => this.signIn()}>
                                        <span className="buttonText">{data["LoginModule"]["LoginScreen"].components.Button}</span>
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div className="lower" >
                        <span className="bottomText">{data["LoginModule"]["LoginScreen"].components.Bottom_Text}</span>
                        <span className="bottomText bottomText2" onPress={() => this.props.navigation.navigate('signup')}>{data["LoginModule"]["LoginScreen"].components.Bottom_Link}</span>
                    </div >
                </div>
            </div>
        )
    }
}





