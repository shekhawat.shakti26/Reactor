import React from 'react'
import {
  BrowserRouter as Router,
  Route, NavLink, Switch
} from 'react-router-dom';
import ScreenNavBar from './Screens/screenNav.js';
import ExtensionNavBar from './Extensions/ExtensionNav.js';
import PushNavBar from './Push/PushNav.js';
import SettingNavBar from './Settings/SettingsNav.js';
import StyleNavBar from './Styles/StyleNav.js';
import './Sidebar.css';
// Each logical "route" has two components, one for
// the sidebar and one for the main area. We want to
// render both of them in different places when the
// path matches the current URL.
const routes = [
  // {
  //   path: '/News_App/styles/',
  //   sidebar: () => <StyleNavBar />,
  //   main: () => <StyleNavBar />
  // },
  {
    path: '/News_App/push/',
    exact: true,
    sidebar: () => <PushNavBar />,
    main: () => <PushNavBar />
  },
  {
    path: '/News_App/settings/',
    exact: true,
    sidebar: () => <SettingNavBar />,
    main: () => <SettingNavBar />
  },
  // {
  //   path: '/News_App/extensions/',
  //   exact: true,
  //   sidebar: () => <ExtensionNavBar />,
  //   main: () => <ExtensionNavBar />
  // },
  // {
  //   path: '/News_App/screen',
  //   exact: true,
  //   sidebar: () => <ScreenNavBar />,
  //   main: () => <ScreenNavBar />
  // },
]


const Sidebar = () => (
  <div >
    <div id="mySidenav" style={{ width: 90, }} className="sidenav">
      {/* Sidebar menu items in left most side */}
      {/* <NavLink style={{ textDecoration: 'none' }} to='/News_App/screen/' activeClassName="active" className="link-container">
        <div className="link" >
          <i className="fa fa-lg fa-copy" />
          <span style={{ marginTop: 4 }}>Screens</span>
        </div>
      </NavLink> */}
      {/* <NavLink style={{ textDecoration: 'none' }} to='/News_App/styles/' activeClassName="active" className="link-container">
        <div className="link" >
          <i className="fa fa-lg fa-paint-brush" />
          <span style={{ marginTop: 4 }}>Styles</span>
        </div>
      </NavLink> */}
      <NavLink style={{ textDecoration: 'none' }} to='/News_App/push/' className="link-container" activeClassName="active">
        <div className="link" >
          <i className="fa fa-lg fa-rss" />
          <span style={{ marginTop: 4 }}>Push</span>
        </div>
      </NavLink>
      <NavLink style={{ textDecoration: 'none' }} to='/News_App/settings/' className="link-container" activeClassName="active" >
        <div className="link" >
          <i className="fa fa-lg fa-spin fa-cog" />
          <span style={{ marginTop: 4 }}>Settings</span>
        </div>
      </NavLink>
      {/* <NavLink style={{ textDecoration: 'none' }} activeClassName="active" to='/News_App/extensions/' className="link-container">
        <div className="link" >
          <i className="fa fa-lg fa-puzzle-piece" />
          <span style={{ marginTop: 4 }}>Extensions</span>
        </div>
      </NavLink> */}
      <Router>
        <div>
          <Switch>
            <Route exact path="/sidebarsetting" component={Sidebar} />
          </Switch>
        </div>
      </Router>
    </div>
    <div className="NavContent">
      <Switch>
        {routes.map((route, index) => (  //display components based on the selected route item
          <Route
            key={index}
            path={route.path}
            exact={route.exact}
            component={route.main}
          />
        ))}
      </Switch>
    </div>

  </div>
)

export default Sidebar