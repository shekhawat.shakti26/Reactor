
import React from 'react';
import './Footer.css';

class Footer extends React.Component {
  render() {
    return (
      <div className="root">
        <div className="container">
          <span className="text">© You Start</span>
          <span className="spacer">·</span>
          <span className="link"> Home</span>
          <span className="spacer">·</span>
          <span className="link"> About</span>
          <span className="spacer">·</span>
          <span className="link">Privacy</span>
          <span className="spacer">·</span>
          <span className="link">Not Found</span>
        </div>
      </div>
    );
  }
}

export default Footer;