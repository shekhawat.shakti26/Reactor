export let makeappJson = {
    "appName": "",
    "userId": "",
    "projectId": "",
    "Splash": "",
    "Icon": "",
    "Database": {
                "SelectedDatabase": "",
                "Firebase": {
                    "apiKey": "AIzaSyD2t3ktLCOJgL7gsXt13-pIsS45migdIsM",
                    "authDomain": "foodie-9d47e.firebaseapp.com",
                    "databaseURL": "https://foodie-9d47e.firebaseio.com",
                    "projectId": "foodie-9d47e",
                    "storageBucket": "foodie-9d47e.appspot.com",
                    "messagingSenderId": "107751693378"
                },
                "Wordpress": {
                    "url": "https://prashant-pandeyme.000webhostapp.com"
                }
            },
    "key": "",
    "Screens":[
        { "Name": "Navigation", "Path": "/News_App/screen/Navigation" },
    ],
    "Theme": {
        "StatusBar": "#a50000 ",
        "BackgroundColor": "#a50000",
        "HeaderColor": "#a50000",
        "HeadingColor": "white",
        "HeadingFont": "serif",
        "TitleColor": "white",
        "TitleFont": "serif",
        "ButtonColor": "white",
        "ButtonTextColor": "#a50000",
        "ButtonTextFont": "serif",
        "NormalTextColor": "black",
        "NormalTextFont": "serif",
        "LinkTextFont": "serif",
        "LinkTextColor": "white",
        "TextInputColor": "white",
        "TextInputBackgroundColor": "rgb(205,97,95)",
        "SubHeadingColor": "black",
        "SubHeadingFont": "serif"
    },
}



// export let makeappJson = {
//     "appName": "",
//     "userId": "",
//     "projectId": "",
//     "Splash": "https://firebasestorage.googleapis.com/v0/b/reactor-2f46b.appspot.com/o/rFuHFaJ07lch1JSH0FvXqIrUXNP2%2Fnewapp052018174731%2Fsplash?alt=media&token=e2302aa9-b775-4514-b6aa-655cd874b186",
//     "Icon": "https://firebasestorage.googleapis.com/v0/b/reactor-2f46b.appspot.com/o/rFuHFaJ07lch1JSH0FvXqIrUXNP2%2Fnewapp052018174731%2FIcon?alt=media&token=060640b7-1ff6-42c6-8c52-90775b42ab1c",
//     "Database": {
//         "SelectedDatabase": "",
//         "Firebase": {
//             "apiKey": "AIzaSyD2t3ktLCOJgL7gsXt13-pIsS45migdIsM",
//             "authDomain": "foodie-9d47e.firebaseapp.com",
//             "databaseURL": "https://foodie-9d47e.firebaseio.com",
//             "projectId": "foodie-9d47e",
//             "storageBucket": "foodie-9d47e.appspot.com",
//             "messagingSenderId": "107751693378"
//         },
//         "Wordpress": {
//             "url": "https://prashant-pandeyme.000webhostapp.com"
//         }
//     },
//     "key": {
//         "url": "https://firebasestorage.googleapis.com/v0/b/reactor-2f46b.appspot.com/o/rFuHFaJ07lch1JSH0FvXqIrUXNP2%2Fnewapp052018174731%2Fmy-release-key.keystore?alt=media&token=29afebaf-de17-46b6-8662-4fb827defb7a",
//         "password1": "shakti123",
//         "password2": "shakti123"
//     },
//     "Screens": [
//         { "Name": "Navigation", "Path": "/News_App/screen/Navigation" },
//        ],
//     "Theme": {
//         "StatusBar": "#a50000 ",
//         "BackgroundColor": "#a50000",
//         "HeaderColor": "#a50000",
//         "HeadingColor": "white",
//         "HeadingFont": "notoserif",
//         "TitleColor": "#ffffff",
//         "TitleFont": "Roboto",
//         "ButtonColor": "#ffffff",
//         "ButtonTextColor": "#a50000",
//         "ButtonTextFont": "Roboto",
//         "NormalTextColor": "black",
//         "NormalTextFont": "serif",
//         "LinkTextFont": "serif",
//         "LinkTextColor": "#0aaad6",
//         "TextInputColor": "white",
//         "TextInputBackgroundColor": "rgb(205,97,95)",
//         "SubHeadingColor": "black",
//         "SubHeadingFont": "serif",
//         "TextInputFont": "serif"
//     },   
  
// }

export function setJson(obj) {
    makeappJson = obj
}

export let previewScreen = "";
export let previewLayout = "";
export function setPreview(Screen, layout) {
    previewScreen = Screen;
    previewLayout = layout;
}