import * as firebase from "firebase";

var config = {
  apiKey: "AIzaSyD1IIsbvPg6AZ61oakFcCZJogMYs8vFOdg",
  authDomain: "reactor-2f46b.firebaseapp.com",
  databaseURL: "https://reactor-2f46b.firebaseio.com",
  projectId: "reactor-2f46b",
  storageBucket: "reactor-2f46b.appspot.com",
  messagingSenderId: "764356928163"
};


const firebaseApp = firebase.initializeApp(config);

export default firebaseApp;