import React from 'react'
import { BrowserRouter as Router, Route, Link, } from 'react-router-dom';
import '../../GlobalStyleSheet.css';
import { Scrollbars } from 'react-custom-scrollbars';
import * as appData from '../../variableData';
var userApp_JSON = localStorage.getItem('JSON');
var AppJson = JSON.parse(userApp_JSON);

const Navigations = [
    {
        path: '/News_App/screen/Navigation/',
        sidebar: () => <div>layout!</div>,
        main: () => <Layout />
    },
]

export default class NavigationScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }
    render() {
        return (
            <Router>
                <div className='MainContainer'>
                    <div className="UpperContainer">
                        <div className="Heading"> <h2>Navigation</h2></div>
                        <div className="TabsDiv">
                            <Link className='links' to="/News_App/screen/Navigation/"> Layout</Link>
                        </div>
                    </div>
                    <Scrollbars style={{ height: 410 }} >
                        <div className="lowerContainer">                      
                            {Navigations.map((route, index) => (
                                <Route
                                    key={index}
                                    path={route.path}
                                    exact={route.exact}
                                    component={route.main}
                                />
                            ))}
                        </div>
                        </Scrollbars>
                </div>
            </Router>
        );
    }
}
function selectLayout(layout, Layout_Id) {
    var userApp_JSON = localStorage.getItem('JSON');
    var AppJson = JSON.parse(userApp_JSON);
    var x = document.getElementById(Layout_Id);
    var y;
    if (layout === 'Layout1') {
        y = document.getElementById("Phone2");
        x.style.backgroundColor = "Black";
        y.style.backgroundColor = "grey";
        AppJson["LoginModule"].Layout = layout;
        appData.setPreview("SignUp", layout);
    }
    if (layout === 'Layout2') {
        y = document.getElementById("Phone1");
        x.style.backgroundColor = "Black";
        y.style.backgroundColor = "grey";
        AppJson["LoginModule"].Layout = layout;
        appData.setPreview("SignUp", layout);
    }
    localStorage.setItem('JSON', JSON.stringify(AppJson));
    console.log("newJson");
    console.log(AppJson);

}

const Layout = () => (
    <div>
Will Be available soon...
        {/* <div className="Products_screen_layout_div" onClick={() => selectLayout("Layout1", "Phone1")}>
            <div className="Products_screen_div" id="Phone1">
                <img className="Products_screen-image" alt="..." src="https://extensions.static.shoutem.com/shoutem/events/1.4.1/server/assets/images/tile-list.png" />
            </div>
            <div className="Products_screen_name">Tile list</div>
        </div>

        <div className="Products_screen_layout_div" onClick={() => selectLayout("Layout2", "Phone2")}>
            <div className="Products_screen_div" id="Phone2">
                <img className="Products_screen-image" alt="..." src="https://extensions.static.shoutem.com/shoutem/events/1.4.1/server/assets/images/compact-list.png" />
            </div>
            <div className="Products_screen_name">Compact list</div>
        </div> */}
        {/* <div className="Products_screen_layout_div ">
            <div className="Products_screen_div">
                <img className="Products_screen-image" src="https://extensions.static.shoutem.com/shoutem/about/1.4.1/server/assets/images/large-photo-clear-navbar.png" />
            </div>
            <div className="Products_screen_name">Large photo with transparent navbar</div>
        </div>

        <div className="Products_screen_layout_div">
            <div className="Products_screen_div">
                <img className="Products_screen-image" src="https://extensions.static.shoutem.com/shoutem/about/1.4.1/server/assets/images/medium-photo-clear-navbar.png" />
            </div>
            <div className="Products_screen_name">Medium photo with transparent navbar</div>
        </div>

        <div className="Products_screen_layout_div">
            <div className="Products_screen_div">
                <img className="Products_screen-image" src="https://extensions.static.shoutem.com/shoutem/about/1.4.1/server/assets/images/large-photo-solid-navbar.png" />
            </div>
            <div className="Products_screen_name">Large photo with solid navbar</div>
        </div>

        <div className="Products_screen_layout_div">
            <div className="Products_screen_div">
                <img className="Products_screen-image" src="https://extensions.static.shoutem.com/shoutem/about/1.4.1/server/assets/images/medium-photo-solid-navbar.png" />
            </div>
            <div className="Products_screen_name">Medium photo with solid navbar</div>
        </div> */}
    </div>
);
