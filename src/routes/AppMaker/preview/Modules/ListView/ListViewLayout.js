import React from 'react';
import './ListViewLayout.css';

var userApp_JSON = localStorage.getItem('JSON');
var AppJson = JSON.parse(userApp_JSON);
var data = AppJson;
var arr = [
    {
        text: "U.N. vote on Jerusalem Israeli ",
        time: "1 min ago",
        image: "http://www.thehindu.com/news/international/article19726014.ece/alternates/FREE_660/UNITEDNATIONSGENERALASSEMBLY",
        newspaper: "Times of India",
        content: "(CNN)-The United Nations voted overwhelmingly to condemn President Donald Trump's decision to recognize Jerusalem as the capital of Israel despite threats from the US to pull funding from the world body.Some 128 countries voted for the resolution, while nine voted 'no', and 35 nations abstained, including Canada, Mexico and Australia.The vote came after US Ambassador to the UN Nikki Haley issued a direct threat, saying that the US will think twice about funding the world body if it voted to condemn Trump's decision.'The United States will remember this day in which it was singled out for attack in this assembly,' Haley said. 'We will remember it when we are called upon to once again make the world's largest contribution' to the UN and when other member nations ask Washington 'to pay even more and to use our influence for their benefit.",
    },
    {
        text: "Lobbying intensifies for India’s UNGA vote on Jerusalem",
        time: "20 min ago",
        image: "http://www.thehindu.com/news/national/article22122695.ece/alternates/FREE_660/RAJASPECTRUMCASE",
        newspaper: 'India Now',
        content: "(CNN)-The United Nations voted overwhelmingly to condemn President Donald Trump's decision to recognize Jerusalem as the capital of Israel despite threats from the US to pull funding from the world body.Some 128 countries voted for the resolution, while nine voted 'no', and 35 nations abstained, including Canada, Mexico and Australia.The vote came after US Ambassador to the UN Nikki Haley issued a direct threat, saying that the US will think twice about funding the world body if it voted to condemn Trump's decision.'The United States will remember this day in which it was singled out for attack in this assembly,' Haley said. 'We will remember it when we are called upon to once again make the world's largest contribution' to the UN and when other member nations ask Washington 'to pay even more and to use our influence for their benefit.",
    },
    {
        text: "A.Raja,acquitted in 2G spectrum allocation cases", time: "1 hour ago",
        content: "(CNN)-The United Nations voted overwhelmingly to condemn President Donald Trump's decision to recognize Jerusalem as the capital of Israel despite threats from the US to pull funding from the world body.Some 128 countries voted for the resolution, while nine voted 'no', and 35 nations abstained, including Canada, Mexico and Australia.The vote came after US Ambassador to the UN Nikki Haley issued a direct threat, saying that the US will think twice about funding the world body if it voted to condemn Trump's decision.'The United States will remember this day in which it was singled out for attack in this assembly,' Haley said. 'We will remember it when we are called upon to once again make the world's largest contribution' to the UN and when other member nations ask Washington 'to pay even more and to use our influence for their benefit.",
        image: "http://www.thehindu.com/incoming/article22122043.ece/alternates/FREE_660/Untitledpng", newspaper: 'Hindustan Times'
    },
    {
        text: "U.N. vote on Jerusalem Israeli ", time: "1 hour ago",
        content: "(CNN)-The United Nations voted overwhelmingly to condemn President Donald Trump's decision to recognize Jerusalem as the capital of Israel despite threats from the US to pull funding from the world body.Some 128 countries voted for the resolution, while nine voted 'no', and 35 nations abstained, including Canada, Mexico and Australia.The vote came after US Ambassador to the UN Nikki Haley issued a direct threat, saying that the US will think twice about funding the world body if it voted to condemn Trump's decision.'The United States will remember this day in which it was singled out for attack in this assembly,' Haley said. 'We will remember it when we are called upon to once again make the world's largest contribution' to the UN and when other member nations ask Washington 'to pay even more and to use our influence for their benefit.",
        image: "http://www.thehindu.com/news/international/article22130056.ece/alternates/FREE_660/PHILIPPINESTRUMPJERUSALEM", newspaper: 'India Now'
    },
    {
        text: "Lobbying intensifies for India’s UNGA vote on Jerusalem", time: "5 hour ago",
        content: "(CNN)-The United Nations voted overwhelmingly to condemn President Donald Trump's decision to recognize Jerusalem as the capital of Israel despite threats from the US to pull funding from the world body.Some 128 countries voted for the resolution, while nine voted 'no', and 35 nations abstained, including Canada, Mexico and Australia.The vote came after US Ambassador to the UN Nikki Haley issued a direct threat, saying that the US will think twice about funding the world body if it voted to condemn Trump's decision.'The United States will remember this day in which it was singled out for attack in this assembly,' Haley said. 'We will remember it when we are called upon to once again make the world's largest contribution' to the UN and when other member nations ask Washington 'to pay even more and to use our influence for their benefit.",
        image: "http://www.thehindu.com/news/international/article19726014.ece/alternates/FREE_660/UNITEDNATIONSGENERALASSEMBLY", newspaper: 'India Now'
    },
    {
        text: "Prosecution's quality totally deteriorated by the end", time: "6 hours ago",
        content: "(CNN)-The United Nations voted overwhelmingly to condemn President Donald Trump's decision to recognize Jerusalem as the capital of Israel despite threats from the US to pull funding from the world body.Some 128 countries voted for the resolution, while nine voted 'no', and 35 nations abstained, including Canada, Mexico and Australia.The vote came after US Ambassador to the UN Nikki Haley issued a direct threat, saying that the US will think twice about funding the world body if it voted to condemn Trump's decision.'The United States will remember this day in which it was singled out for attack in this assembly,' Haley said. 'We will remember it when we are called upon to once again make the world's largest contribution' to the UN and when other member nations ask Washington 'to pay even more and to use our influence for their benefit.",
        image: "http://www.thehindu.com/news/national/article22122695.ece/alternates/FREE_660/RAJASPECTRUMCASE", newspaper: 'India Now'
    },
    {
        text: "Analysts aren't Buying the sterling Rebound", time: "7 hour ago",
        content: "(CNN)-The United Nations voted overwhelmingly to condemn President Donald Trump's decision to recognize Jerusalem as the capital of Israel despite threats from the US to pull funding from the world body.Some 128 countries voted for the resolution, while nine voted 'no', and 35 nations abstained, including Canada, Mexico and Australia.The vote came after US Ambassador to the UN Nikki Haley issued a direct threat, saying that the US will think twice about funding the world body if it voted to condemn Trump's decision.'The United States will remember this day in which it was singled out for attack in this assembly,' Haley said. 'We will remember it when we are called upon to once again make the world's largest contribution' to the UN and when other member nations ask Washington 'to pay even more and to use our influence for their benefit.",
        image: "https://www.w3schools.com/howto/img_avatar.png", newspaper: 'India Now'
    },
];

export class Layout1 extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }
    render() {
        return (
            <div className="NewsFeed_screen">
                <div className="NewsFeed_titleContainer">
                    <span className="NewsFeed_title">Top Stories</span>
                </div>
                <ul className="NewsFeed_ul">
                    {arr.map((x) => {
                        return (
                            <li key={x} className="NewsFeed_list">
                                <div className="NewsFeed_list_item">

                                    <div className="NewsFeed_left">
                                        <span style={{ flex: 7, }}>{x.text}</span>
                                        <span style={{ flex: 3, color: "gray", fontSize: 10, }}>{x.time}</span>
                                    </div>

                                    <div className="NewsFeed_image_container">

                                        <img src={x.image} alt="..." className="NewsFeed_image" />

                                    </div>
                                </div>
                            </li>
                        )
                    })}
                </ul>
            </div>
        )
    }
}

export class Layout2 extends React.Component {
    render() {
        return (
            <div className="NewsFeed_screen">
                <div className="NewsFeed_titleContainer">
                    <span className="NewsFeed_title">Top Stories</span>
                </div>
                <ul className="NewsFeed_Layout2_ul">
                    {arr.map((x) => {
                        return (
                            <li key={x} className="NewsFeed_Layout2_list">
                                <div className="NewsFeed_Layout2_list_item">
                                    <div className="NewsFeed_Layout2_top">
                                        <span style={{ flex: 4, }}>{x.text}</span>
                                        <span style={{ flex: 6, color: "gray", fontSize: 8, }}>{x.content.substring(0, 90)} ...</span>
                                    </div>

                                    <div className="NewsFeed_Layout2_image_container">

                                        <img src={x.image} alt="..." className="NewsFeed_Layout2_image" />

                                    </div>
                                </div>
                            </li>
                        )
                    })}
                </ul>
            </div>
        )
    }
}