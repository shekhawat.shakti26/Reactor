import React from 'react';
import './Navigation.css';
import {Link} from 'react-router-dom';

class Navigation extends React.Component {
  render() {
    return (
      <div className="Navigation-root" role="navigation">
        <Link className="Navigation-link" to="/about">
          About
        </Link>
        <span className="Navigation-spacer"> | </span>
        <Link className="Navigation-link" to="/contact">
          Contact
        </Link>
        <span className="Navigation-spacer"> | </span>
        <Link className="Navigation-link" to="/login">
          Log in
        </Link>
        <span className="Navigation-spacer"> | </span>
        <Link className="Navigation-link" to="/SignUp">
          Sign Up
        </Link>
        {/* <span className="Navigation-spacer"> | </span>
        <Link className="Navigation-link" to="/ui">
          Create App
        </Link> */}
      </div>
    );
  }
}

export default Navigation;