import React, { Component } from 'react';
import {View,Container, Header, Content, Card, CardItem, Text, Body,Left,Right,Button,Icon,Title } from 'native-base';
import {
  AppRegistry,
  Platform,
  StyleSheet,
  StatusBar,
  Image
} from 'react-native';

export class DetailViewScreen extends Component{
    static navigationOptions = {
        header:null,
      };

      constructor(props) {
        super(props);
        this.state = { 
            text:this.props.navigation.state.params.text,
            time:this.props.navigation.state.params.time,
            Image:this.props.navigation.state.params.imageUrl,
            content:this.props.navigation.state.params.content,
        }
    }
            
    
  render() {
    return (
      <Container style={styles.container}>
          <StatusBar
                backgroundColor="#a50000"
                barStyle="light-content"
            />
        <Header style={styles.header}>
          <Left>
            <Button style={styles.button} onPress={()=>this.props.navigation.navigate('cardview')}>
              <Icon name='arrow-back'/>
              </Button>
            </Left>
            <Body >
              <Title >News</Title>
              </Body>
              <Right >
              </Right>
          </Header>
          <Content >
          <Text style={{paddingTop:10,paddingLeft:15,fontSize:20,fontWeight:'500',height:"auto"}}>{this.state.text}:</Text>
              <View style={{width:'100%',flex:1,paddingLeft:2.5,paddingRight:2.5}}>
              <Image style={{width:'100%',height:250}}source={{uri:this.state.Image}} />              
              </View>
        <Text style={{paddingLeft:15}}>{this.state.content}</Text>
          </Content>
        </Container>
    );
  }
}
const styles=StyleSheet.create({
    container:{
        flexDirection:'column'
    },
    header:{
        backgroundColor:"#a50000",
        flexDirection:'row'
    },
    button:{
        backgroundColor:'#a50000'
    }
});
